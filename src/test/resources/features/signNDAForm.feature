@signNDAForm @smoke

  Feature: Sign NDA Form

    Scenario: Contractor e-Signing NDA
      Given I login to Gmail
      When I'm waiting for 20 seconds
      Then I open inbox email with subject Confidentiality and NDA - Signature requested by SimplifyVMS Test
      And I sign the NDA form
      And I check acknowledgement is shown with text Thanks for signing the document