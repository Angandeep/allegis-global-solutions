@approveJobs

Feature: Approve Jobs

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Approve job as Reporting Manager - VP
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Jobs Pending Approval in dashboard
    Then I select the approval job with status Pending Approval - Open
    And I go to Workflow Approvals page
    And I Approve the job
    And I check success alert is shown with message You have approved job successfully.

  @regression
  Scenario: Approval by “Hiring Manager”
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Jobs Pending Approval in dashboard
    Then I select the approval job with status Pending Approval - Open
    And I go to Workflow Approval page
    And I Approve the job
    And I check success alert is shown with message You have approved job successfully.