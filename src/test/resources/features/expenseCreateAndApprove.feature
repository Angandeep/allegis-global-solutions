@expenseCreateAndApprove @smoke

Feature: Expense Create And Approve

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

   Scenario: Vendor creating Expense
     Given I impersonate as Vendor with name Kelly Staffing Agency
     When I click on Add Expense in dashboard
     Then I select location as USA-Dallas and contractor as Frazer Dan-Webb Mc'Gee
     And I select June ( 06/08/2020 to 06/14/2020 ) as Month & Week and continue
     And I add expense for Monday 06/08/2020 with type Car Rental for amount 30.00
     And I check success alert is shown with message Expense item added successfully.
     And I'm clicking on Submit For Approval
     And I'm on the Expenses page

  Scenario: Timesheet Approving Manager Approve Expense
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Expense Pending Approval in dashboard
    Then I'm selecting Expense with name Frazer Dan-Webb Mc'Gee and status Pending
    And I'm clicking on Approve Expense
    And I check success alert is shown with message Approved