@createJobs

Feature: Create Jobs

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Create job as HM
    Given I impersonate as Hiring Manager with name Angela Ross
    When I add job with title Audit Manager for USA-Dallas with Level as Level - I
    Then I select pre-identified Yes with name ABC and Vendor XYZ and No expense with No Virtual Work
    And I check success alert is shown with message was successfully submitted.

  @regression
  Scenario: Create job as PMO
    Given I add job with title Java Developer III for USA-CA with Level as Level - III
    When I select pre-identified Yes with name Phil and the Vendor as Coulson
    Then I select Hiring Manager as Angela Ross and Yes Expense with No Virtual Work
    And I check success alert is shown with message was successfully submitted.