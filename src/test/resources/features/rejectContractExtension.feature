@rejectContractEntension @regression

Feature: Contract Extension

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Hiring Manager Reject Contract Extension
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract Extensions Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I reject the offer
    And I select reason for reject as Incorrect Workorder details and comments as DEF
    And I check danger alert is shown with message You have rejected workflow successfully.
    And I check that contract status is Rejected

  Scenario: VP Reject Contract Extension
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Contract Extensions Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I reject the offer
    And I select reason for reject as Incorrect Workorder details and comments as DEF
    And I check danger alert is shown with message You have rejected workflow successfully.
    And I check that contract status is Rejected

  Scenario: Vendor Reject Contract Extension
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Contract Extension in dashboard
    Then I select contractor with name ABC XYZ and status Supplier Pending
    And I reject the offer
    And I select reason for reject as Contractor not available and comments as DEF
    And I check danger alert is shown with message You have rejected workflow successfully.
    And I check that contract status is Rejected