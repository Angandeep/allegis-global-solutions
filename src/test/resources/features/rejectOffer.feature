@rejectOffer @regression

Feature: Reject Offer

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Reject Offer by Hiring Manager
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Offers Pending Approval in dashboard
    Then I select the approval job with status Offer - Pending Approval
    And I Reject the offer
    And I give rejection reason as Additional Budget Not Available and notes as DEF
    And I check success alert is shown with message Workflow Rejected Successfully.
    And I check that offer status is Rejected

  Scenario: Reject Offer by Vendor
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Offers in dashboard
    Then I select the approval job with status Waiting for Vendor Approval
    And I Reject Offer the offer
    And I select reason for Reject as Candidate is No Longer Available and comments as DEF
    And I check success alert is shown with message You have successfully rejected the offer.
    And I check that offer status is Rejected