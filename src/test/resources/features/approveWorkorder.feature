@approveWorkorder

Feature: Approve Workorders

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Hiring Manager Approving Work Order
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Workorders Pending Approval in dashboard
    Then I select the approval job with status Pending Approval - HM
    And I go to Workorder Workflow page
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.
    And I check that offer status is Pending Approval - Supplier

  @smoke
  Scenario: Vendor Approving Work Order
    When I impersonate as Vendor with name Kelly Staffing Agency
    Then I click on Pending Workorders in dashboard
    Then I select the approval job with status Pending Approval - Supplier
    And I fill background checklist and save
    And I check success alert is shown with message You have successfully submitted Background Check List & Onboarding Information
    And I'm clicking on Accept Workorder
    And I check success alert is shown with message You have successfully accepted the Workorder