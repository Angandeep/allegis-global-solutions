@timesheetCreateAndApprove @smoke

Feature: Timesheet Create And Approve

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Contractor Creating Timesheet
    Given I login to Gmail
    When I'm waiting for 30 seconds
    Then I open inbox email with subject Allegis Global Solutions Timesheet Login Details
    And I login as Contractor
    And I change password to Welcome123!
    And I'm clicking on Add TImesheet
    And I select June ( 06/22/2020 to 06/28/2020 ) as Month & Week and continue
    And I set in time as 08:00 and out time as 18:00 for week
    And I check total hours is 50:00 Hour(s)
    And I check success alert is shown with message Timesheet saved successfully.
    And I'm clicking on Submit Timesheet
    And I check success alert is shown with message Timesheet submitted successfully.

  Scenario: Vendor creating Timesheet
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Add Timesheet in dashboard
    Then I select Adela H Price as Contractor and continue
    And I select June ( 06/15/2020 to 06/21/2020 ) as Month & Week and continue
    And I set in time as 08:00 and out time as 18:00 for week
    And I check success alert is shown with message Timesheet saved successfully.
    And I'm clicking on Submit Timesheet
    And I check success alert is shown with message Timesheet submitted successfully.

    #Bug AGS-1026 & AGS-1027 found, so created code with ICEE details. It won't run in AGS.

  Scenario: Timesheet Approving Manager Approve Timesheet
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Timesheets Pending Approval in dashboard
    Then I'm selecting Contractor with name Adela H Price and status Pending
    And I'm clicking on Approve Timesheet
    And I check success alert is shown with message Timesheet Approved Successfully.