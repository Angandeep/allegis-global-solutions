@rejectTimesheet @regression

Feature: Reject Timesheet

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

#Bug AGS-1026 & AGS-1027 found, so created code with ICEE details. It won't run in AGS.

  Scenario: Timesheet Approving Manager Reject Timesheet
    Given I impersonate as Hiring Manager with name Adam Couillard
    When I click on Timesheets Pending Approval in dashboard
    Then I'm selecting Contractor with name Jonty Rhodes and status Pending
    And I'm clicking on Reject Timesheet
    And I give Reject reason as Incorrect Time Submitted and notes as DEF
    And I check success alert is shown with message Timesheet Rejected Successfully.