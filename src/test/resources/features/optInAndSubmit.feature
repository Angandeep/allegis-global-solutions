@optInAndSubmit

Feature: Opt In And Submit

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Opt-In and Submit as Vendor
    When I impersonate as Vendor with name Kelly Staffing Agency
    And I click on New Job Requests in dashboard
    And I select the approval job with status Sourcing
    And I Opt-In the job
    And I check success alert is shown with message Opted-in successfully.
    And I Add Submission the job
    And I fill the Submission Form
    And I check success alert is shown with message has been submitted