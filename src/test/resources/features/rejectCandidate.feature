@rejectCandidate   @regression

Feature: Reject Candidate

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Candidate Rejected by PMO
    Given I click on Interviews in dashboard
    When I select the created job with status Approve
    Then I'm clicking on reject candidate
    And I select reason for Rejection as Negative | No Show to Interview and comments as ABC
    And I check success alert is shown with message You have successfully rejected the candidate.
    And I check the status of job with name QA Engineer II is Cancelled