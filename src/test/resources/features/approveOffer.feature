@approveOffer

Feature: Approve Offers

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Approve Offer by Hiring Manager
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Offers Pending Approval in dashboard
    And I select the approval job with status Offer - Pending Approval
    And I Approve the offer
    And I check that Action status is Approved
    And I impersonate back as the Program Manager
    When I impersonate as Reporting Manager with name Don LeBert
    Then I click on Offers Pending Approval in dashboard
    And I select the approval job with status Offer - Pending Approval
    And I Approve the offer
    And I check that offer status is PMO - Review