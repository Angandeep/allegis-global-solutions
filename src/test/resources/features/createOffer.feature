@createOffer

Feature: Create Offer

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Create Offer by PMO
    Given I click on Interviews in dashboard
    When I select the created job with status Interview Completed
    Then I'm clicking on Create Offer
    And Job Offer is created
    And I check success alert is shown with message You have successfully created the Offer
    And I check the status of job with name Audit Manager is Offer - Pending Approval