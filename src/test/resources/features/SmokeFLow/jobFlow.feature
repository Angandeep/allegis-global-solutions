@jobFlow @smokeTest

Feature: Total flow of Job

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Create job as HM
    Given I impersonate as Hiring Manager with name Angela Ross
    When I add job with title Audit Manager for USA-Dallas with Level as Level - I
    Then I select pre-identified Yes with name DEF and Vendor UVW and Yes expense with No Virtual Work
    And I check success alert is shown with message was successfully submitted.

  Scenario: PMO Submit for Approval
    When I click on Jobs Pending Review in dashboard
    And I select the created job with name Audit Manager
    Then I check the status is Pending - PMO
    And I Submit For Approval the job
    And I check success alert is shown with message Job Updated Successfully.
    Then I check the status is Pending Approval - Open

  Scenario: Approve job as Reporting Manager - VP
    And I impersonate as Reporting Manager with name Don LeBert
    And I click on Jobs Pending Approval in dashboard
    And I select the created job with name Audit Manager
    And I go to Workflow Approvals page
    And I Approve the job
    And I check success alert is shown with message You have approved job successfully.

  Scenario: Release to Vendor as PMO
    Then I click on Jobs Pending Release in dashboard
    And I select the created job with name Audit Manager
    Then I check the status is Open
    And I update job as Release to Vendor
    And I check success alert is shown with message The job request has been released successfully

  Scenario: Opt-In and Submit as Vendor
    When I impersonate as Vendor with name Kelly Staffing Agency
    And I click on Jobs Pending Review in dashboard
    And I select the created job with name Audit Manager
    Then I check for Vendor the status is Sourcing
    And I Opt-In the job
    And I check success alert is shown with message Opted-in successfully.
    And I Add Submission the job
    And I fill the Submission Form
    And I check success alert is shown with message has been submitted

  Scenario: Submission Review by PMO and shortlist
    When I click on Resumes To Review in dashboard
    Then I select the reviewing resume with name Audit Manager
    And I check the status is Sourcing
    And I go to Submissions page
    And I check that submission status is Submitted
    And I'm clicking on Action
    And I check success alert is shown with message The following status has been updated to MSP Reviewed.
    And I check that submission status is MSP Reviewed
    And I'm clicking on Shortlist
    And I check success alert is shown with message You have successfully Shortlisted a candidate
    And I check that submission status is Shortlisted

  Scenario: Submission Review by Hiring Manager and Schedule Interview
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Resumes To Review in dashboard
    Then I select the shortlisted resume with name Audit Manager
    And I check that submission status is Client Review
    And I'm clicking on Schedule Interview
    And I schedule Interview with duration 30 Minutes and type Personal Interview
    And I select slot as 10:00 AM - 10:30 AM
    And I check success alert is shown with message Your Interview Posting was successfully submitted.
    And I check that submission status is Waiting for Approval

  Scenario: Accept Interview by Vendor
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Interviews in dashboard
    Then I select Interview Job with name Audit Manager
    And I check that submission status is Waiting for Approval
    And I Accept interview
    And I check success alert is shown with message You have successfully accepted the interview.
    And I check that interview status is Approved

  Scenario: Interview Completed by Hiring Manager
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Post-Interview Actions in dashboard
    And I go to All Interviews page
    Then I select Approved Interview Job with name Audit Manager
    And I'm clicking on Interview Completed
    And I select reason as Candidate is fit for the job, planning to make an offer
    And I check success alert is shown with message Interview completed successfully.
    And I check that interview status is Interview Completed

  Scenario: Create Offer by PMO
    Given I click on Interviews in dashboard
    Then I select the Interview Completed Job with name Audit Manager
    And I'm clicking on Create Offer
    And Job Offer is created
    And I check success alert is shown with message You have successfully created the Offer
    And I check the status of job with name Audit Manager is Offer - Pending Approval

  Scenario: Approve Offer by Hiring Manager
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Offers Pending Approval in dashboard
    Then I select the Offer - Pending Approval Job with name Audit Manager
    And I Approve the offer
    And I check that Action status is Approved
    And I impersonate back as the Program Manager
    When I impersonate as Reporting Manager with name Don LeBert
    Then I click on Offers Pending Approval in dashboard
    Then I select the Offer - Pending Approval Job with name Audit Manager
    And I Approve the offer
    And I check that offer status is PMO - Review

  Scenario: Release Offer by PMO
    Given I click on Approved Offers Pending PMO Release in dashboard
    Then I select the PMO - Review Job with name Audit Manager
    And I'm clicking on Release Offer
    And I check success alert is shown with message Offer released successfully.

  Scenario: Accept Offer by Vendor
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Offers in dashboard
    Then I select the Waiting for Vendor Approval Job with name Audit Manager
    And I Accept the offer
    And I check success alert is shown with message You have successfully accepted the Offer.
    And I check that offer status is Approved
    Then I impersonate back as the Program Manager
    And I log out

  Scenario: Contractor e-Signing NDA
    Given I login to Gmail
    When I'm waiting for 30 seconds
    Then I open inbox email with subject Confidentiality and NDA - Signature requested by SimplifyVMS Test
    And I sign the NDA form
    And I check acknowledgement is shown with text Thanks for signing the document

  Scenario: PMO creating Workorder
    Given I click on Offers Pending Workorders Creation in dashboard
    When I'm selecting NDA status Completed with name as Audit Manager
    Then I'm clicking on Create Workorder
    And Job Workorder is created
    And I check success alert is shown with message Success! Workorder created successfully.
    And I check that offer status is Pending Approval - HM

  Scenario: Hiring Manager and Vendor Approving Work Order
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Workorders Pending Approval in dashboard
    Then I select the Pending Approval - HM Job with name Audit Manager
    And I go to Workorder Workflow page
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.
    And I check that offer status is Pending Approval - Supplier
    And I impersonate back as the Program Manager
    When I impersonate as Vendor with name Kelly Staffing Agency
    Then I click on Pending Workorders in dashboard
    Then I select the Pending Approval - Supplier Job with name Audit Manager
    And I fill background checklist and save
    And I check success alert is shown with message You have successfully submitted Background Check List & Onboarding Information
    And I'm clicking on Accept Workorder
    And I check success alert is shown with message You have successfully accepted the Workorder

  Scenario: PMO creating Contract
    Given I click on Validate Onboarding Completion in dashboard
    When I select the Approved Job with name Audit Manager
    Then I go to Onboarding Info page
    And I select Timesheet Type as Timesheet with Clock System
    And I check success alert is shown with message The Contractor has been successfully onboarded. Contractor Login information has been emailed.
    Then I go to Background Verification page
    And I'm clicking on Background Check Reviewed
    And I check success alert is shown with message Background verification marked as reviewed.
    Then I go to Back to List of Workorders page
    When I select job with name Audit Manager and Contract status as OnBoarded

  Scenario: PMO Creating Contract Amendment
    Given I click on Contracts in dashboard
    When I select the Approved Contract with name Audit Manager
    Then I'm clicking on Update Contract
    And I select update reason as Rate Change with effective date as tomorrow
    And I check success alert is shown with message Contract will be updated once the approval process is successful.

  Scenario: Hiring Manager Approve Contract Amendment
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract Amendments Pending Approval in dashboard
    Then I select contractor with name DEF UVW and status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.

  Scenario: VP approving Contract Amendment
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Contract Amendments Pending Approval in dashboard
    Then I select contractor with name DEF UVW and status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.
    And I check that contract status is Approved

  Scenario: Contractor Creating Timesheet
    Given I login to Gmail
    When I'm waiting for 30 seconds
    Then I open inbox email with subject Allegis Global Solutions Timesheet Login Details
    And I login as Contractor
    And I change password to Welcome123!
    And I'm clicking on Add TImesheet
    And I select June ( 06/15/2020 to 06/21/2020 ) as Month & Week and continue
    And I set in time as 08:00 and out time as 18:00 for week
    And I check total hours is 50:00 Hour(s)
    And I check success alert is shown with message Timesheet saved successfully.
    And I'm clicking on Submit Timesheet
    And I check success alert is shown with message Timesheet submitted successfully.

  Scenario: Vendor creating Timesheet
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Add Timesheet in dashboard
    Then I select DEF UVW as Contractor and continue
    And I select June ( 06/22/2020 to 06/28/2020 ) as Month & Week and continue
    And I set in time as 08:00 and out time as 18:00 for week
    And I check total hours is 50:00 Hour(s)
    And I check success alert is shown with message Timesheet saved successfully.
    And I'm clicking on Submit Timesheet
    And I check success alert is shown with message Timesheet submitted successfully.

  Scenario: Timesheet Approving Manager Approve Timesheet
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Timesheets Pending Approval in dashboard
    Then I'm selecting Contractor with name DEF UVW and status Pending
    And I check Total Hours is 50.00 and Amount is $2000.00
    And I check timesheet approving manager is Angela Ross
    And I'm clicking on Approve Timesheet
    And I check success alert is shown with message Timesheet Approved Successfully.

  Scenario: Vendor creating Expense
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Add Expense in dashboard
    Then I select location as USA-Dallas and contractor as DEF UVW
    And I select June ( 06/24/2020 to 06/28/2020 ) as Month & Week and continue
    And I add expense for Wednesday 06/24/2020 with type Car Rental for amount 30.00
    And I check success alert is shown with message Expense item added successfully.
    And I'm clicking on Submit For Approval
    And I'm on the Expenses page

  Scenario: Timesheet Approving Manager Approve Expense
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Expense Pending Approval in dashboard
    Then I'm selecting Expense with name DEF UVW and status Pending
    And I'm clicking on Approve Expense
    And I check success alert is shown with message Approved