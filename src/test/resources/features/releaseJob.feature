@releaseJob

Feature: Release Jobs

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Release to Vendor as PMO
    Given I click on Jobs Pending Release in dashboard
    When I select the created job with status Open
    And I update job as Release to Vendor
    And I check success alert is shown with message The job request has been released successfully