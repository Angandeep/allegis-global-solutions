package Config;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        tags= {"@regression"},
        features= "./src/test/resources/features",
        glue= {"Steps","Config"},
        monochrome = true)

public class Runner {
}