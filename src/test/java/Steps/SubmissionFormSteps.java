package Steps;

import Base.BasePage;
import Pages.SubmissionFormPage;
import cucumber.api.java.en.Then;

public class SubmissionFormSteps {

    SubmissionFormPage addSubmissionPage = new SubmissionFormPage(BasePage.driver);

    @Then("^I fill the Submission Form$")
    public void fillSubmissionForm() throws InterruptedException {
        System.out.println("Filling submission form with all details.");
        addSubmissionPage.fillSubmissionForm();
        System.out.println("Submission form filled.");
    }
}