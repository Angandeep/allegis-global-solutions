package Steps;

import Base.BasePage;
import Pages.ContractsPage;
import Pages.InterviewApprovalPage;
import Pages.OfferApprovalPage;
import Pages.WorkOrderCreatePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class interviewApprovalSteps{

    InterviewApprovalPage InterviewApprovalPage = new InterviewApprovalPage(BasePage.driver);
    OfferApprovalPage offerApprovalPage = new OfferApprovalPage(BasePage.driver);
    WorkOrderCreatePage workOrderCreatePage = new WorkOrderCreatePage(BasePage.driver);
    ContractsPage contractsPage = new ContractsPage(BasePage.driver);

    @And("^I select Interview Job with name (.+)$")
    public void selectInterviewJob(String jobName) throws InterruptedException {
        System.out.println("Selecting job with name: " + jobName);
        InterviewApprovalPage.openInterviewJobDetails(jobName);
        System.out.println(jobName + " selected");
    }

    @And("^I (.+) interview$")
        public void interViewStatusChange(String status) throws InterruptedException {
        System.out.println(status + "ing the interview");
        if (status.equals("Accept")) {
            InterviewApprovalPage.acceptInterview();
        }
        if (status.equals("Schedule")) {
            InterviewApprovalPage.pmoScheduleInterview();
        }
        System.out.println(status + "ed interview");
    }

    @Then("^I check that interview status is (.+)$")
    public void checkInterviewStatus(String status) {
        System.out.println("Checking interview status is ..." + status);
        if (status.equals("Approved")) {
            Assert.assertEquals(status, InterviewApprovalPage.checkInterviewApproved());
        }
        if (status.equals("Interview Completed")) {
            Assert.assertEquals(status, InterviewApprovalPage.checkInterviewCompleted());
        }
        System.out.println("Status verified as: " + status);
    }

    @And("^I select (.+)ed Interview Job with name (.+)$")
    public void openInterviewWithStatus(String status, String jobName) throws InterruptedException {
        System.out.println("Selecting job with name: " + jobName+ " and status as " +status + "ed");
        InterviewApprovalPage.openInterviewWithStatus(status,jobName);
        System.out.println(jobName + " selected");
    }

    @Then("^I select reason as (.+)$")
    public void setInterviewCompleteReason(String option) throws InterruptedException {
        System.out.println("Updating reason as " + option);
        InterviewApprovalPage.interviewComplete(option);
        System.out.println("Updated job as " + option);
    }

    @And("^I select the (.+) Job with name (.+)$")
    public void selectJobWithStatus(String status, String jobName) throws InterruptedException {
        System.out.println("Selecting job with name: " + jobName+ " and status as " +status);
        if (status.equals("Interview Completed")) {
            InterviewApprovalPage.selectPmoInterviewWithStatus(status, jobName);
        }
        if (status.equals("Offer - Pending Approval")) {
            offerApprovalPage.selectOfferJobWithStatus(status, jobName);
        }
        if (status.equals("PMO - Review")) {
            offerApprovalPage.selectPmoOfferJobWithStatus(status, jobName);
        }
        if (status.equals("Waiting for Vendor Approval")) {
            offerApprovalPage.selectVendorOfferJobWithStatus(status, jobName);
        }
        if (status.equals("Pending Approval - HM")) {
            workOrderCreatePage.selectHmPendingWorkOrder(status, jobName);
        }
        if (status.equals("Pending Approval - Supplier")) {
            workOrderCreatePage.selectVendorPendingWorkOrder(status, jobName);
        }
        if (status.equals("Approved")) {
            workOrderCreatePage.selectHmPendingWorkOrder(status, jobName);
        }
        if (status.equals("Approve")) {
            workOrderCreatePage.selectPmoPendingWorkOrder(status, jobName);
        }
        System.out.println(jobName + " selected");
    }

    @Then("^I check the status of job with name (.+) is (.+)$")
    public void checkJobStatus(String jobName, String status) throws InterruptedException {
        Assert.assertEquals(status, InterviewApprovalPage.checkJobStatus(jobName));
        System.out.println("Status is " + status);
    }
}