package Steps;

import Base.BasePage;
import Pages.SignNDAPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class signNDASteps {

    SignNDAPage signNDAPage = new SignNDAPage(BasePage.driver);

    @Then("^I login to Gmail$")
    public void loginToGmail(){
        System.out.println("Logging in to gmail");
        signNDAPage.loginToGmail();
        System.out.println("Logged in to gmail");
    }

    @And("^I open inbox email with subject (.+)$")
    public void OpenEmail(String subject) {
        System.out.println("Opening email");
        signNDAPage.OpeningEmail(subject);
        System.out.println("email opened with subject : " + subject);
    }

    @And("^I sign the NDA form$")
    public void signNDA() throws InterruptedException {
        System.out.println("Contractor opening and signing NDA form");
        signNDAPage.signNDA();
        System.out.println("Contractor signed NDA form");
    }

    @And("^I check acknowledgement is shown with text (.+)$")
    public void successSign(String message){
        Assert.assertEquals(message, signNDAPage.signAcknowledged(message));
        System.out.println("Success alert shown with message as: " + message );
    }
}