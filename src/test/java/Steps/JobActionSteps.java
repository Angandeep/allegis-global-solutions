package Steps;

import Base.BasePage;
import Pages.InterviewApprovalPage;
import Pages.JobActionPage;
import Pages.SubmissionActionPage;
import Pages.WorkOrderCreatePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class JobActionSteps {
    JobActionPage jobActionPage = new JobActionPage(BasePage.driver);
    SubmissionActionPage submissionActionPage = new SubmissionActionPage(BasePage.driver);
    InterviewApprovalPage interviewApprovalPage = new InterviewApprovalPage(BasePage.driver);
    WorkOrderCreatePage workOrderCreatePage = new WorkOrderCreatePage(BasePage.driver);

    @Then("^I check the status is (.+)$")
        public void checkApprovalStatus(String status){
        System.out.println("Checking status is ..." + status);
        if (status.equals("Rejected")){
            Assert.assertEquals(status, jobActionPage.checkRejectedStatus());
        } else {
            Assert.assertEquals(status, jobActionPage.checkStatus());
        }
        System.out.println("Status verified as: " + status);
    }

    @Then("^I check for Vendor the status is (.+)$")
    public void checkVendorJobStatus(String status){
        System.out.println("Checking that job status is " + status);
        Assert.assertEquals(status, jobActionPage.checkVendorJobStatus());
        System.out.println("Job status " + status + "verified");
    }

    @Then("^I (.+) the job$")
    public void jobStatusChange(String action) throws InterruptedException {
        System.out.println("Changing status by " + action);
        if (action.contains("Submit For Approval")){
            jobActionPage.setSubmitForApproval();
        }
        if (action.equals("Approve")){
        jobActionPage.setJobApprove();
        }
        if (action.equals("Opt-In")){
            jobActionPage.setVendorOptIn();
        }
        if (action.equals("Add Submission")){
            jobActionPage.setVendorAddSubmission();
        }
        if (action.contains("Reject")){
            jobActionPage.setJobReject();
        }
        if (action.contains("reject")){
            jobActionPage.setJobRejection();
        }
        System.out.println("Job status changed to " + action+ "ed");
    }

    @Then("^I give reason as (.+) and comments as (.+)$")
    public void setRejectReason(String option, String note) throws InterruptedException {
        System.out.println("Updating rejection reason as " + option + "and comments as " + note);
        jobActionPage.setJobRejectReason(option, note);
        System.out.println("Updated job as " + option + "and comments as " + note);
    }

    @Then("^I'm giving reason as (.+) and comments as (.+)$")
    public void setRejectionReason(String option, String note) throws InterruptedException {
        System.out.println("Updating rejection reason as " + option + "and comments as " + note);
        jobActionPage.setJobRejectionReason(option, note);
        System.out.println("Updated job as " + option + "and comments as " + note);
    }

    @Then("^I update job as (.+)$")
    public void changeAndUpdateJobStatus(String option) throws InterruptedException {
        System.out.println("Updating job as " + option);
        jobActionPage.setJobStatus(option);
        System.out.println("Updated job as " + option);
    }

    @And("^I go to (.+) page$")
    public void goToPage(String page) {
        System.out.println("Going to '" + page + "' page" );
        if (page.equals("Workflow Approvals")) {
            jobActionPage.goToWorkflowApprovals();
        }
        if (page.equals("Workflow Approval")) {
            jobActionPage.goToWorkflowApproval();
        }
        if (page.equals("Submissions")) {
            submissionActionPage.goToSubmissions();
        }
        if (page.equals("All Interviews")){
            interviewApprovalPage.goToAllInterview();
        }
        if (page.equals("Workorder Workflow")){
            workOrderCreatePage.goToWorkorderWorkflow();
        }
        if (page.equals("Onboarding Info")){
            workOrderCreatePage.goToOnboardingInfo();
        }
        if (page.equals("Background Verification")){
            workOrderCreatePage.goToBackgroundVerification();
        }
        if (page.equals("Back to List of Workorders")){
            workOrderCreatePage.goBackToWorkorders();
        }
        System.out.println("Came to '" + page + "' page");
    }
}