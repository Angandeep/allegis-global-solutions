package Steps;

import Base.BasePage;
import Pages.WorkOrderCreatePage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class WorkOrderCreateSteps {

    WorkOrderCreatePage workOrderCreatePage = new WorkOrderCreatePage(BasePage.driver);

    @When("^I'm selecting NDA status (.+) with name as (.+)$")
    public void selectNDAStatusJob(String status, String jobName) throws InterruptedException {
        System.out.println("Selecting job with name: " + jobName);
        workOrderCreatePage.selectPendingWorkorderWithStatus(status, jobName);
        System.out.println(jobName + " selected");
    }

    @When("^I fill background checklist and save$")
    public void fillBackground() throws InterruptedException {
        System.out.println("Filling Background checklist...");
        workOrderCreatePage.fillBackground();
        System.out.println("Background checklist filled");
    }

    @Then("^I select Timesheet Type as (.+)$")
    public void selectTimesheetType(String type){
        System.out.println("Selecting timesheet type as : " + type);
        workOrderCreatePage.selectTimesheetType(type);
        System.out.println("Selected timesheet type");
    }

    @Then("^I select job with name (.+) and Contract status as (.+)$")
    public void verifyContractStatus(String jobName, String contractStatus) throws InterruptedException {
        System.out.println("Verifying jobname : " + jobName+ " has status : " +contractStatus);
        workOrderCreatePage.verifyContractStatus(jobName, contractStatus);
        System.out.println("Status verified.");
    }

    @When("^I'm selecting (.+) NDA status job$")
    public void selectNDAStatusJob(String status) throws InterruptedException {
        System.out.println("Selecting job with status: " + status);
        workOrderCreatePage.pmoSelectNDAComplete(status);
        System.out.println(status + " selected");
    }
}