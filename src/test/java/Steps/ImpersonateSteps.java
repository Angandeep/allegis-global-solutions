package Steps;

import Base.BasePage;
import Pages.ImpersonatePage;
import cucumber.api.java.en.Then;

public class ImpersonateSteps{
    ImpersonatePage impersonatePage = new ImpersonatePage(BasePage.driver);

    /*
    ** Impersonating users
     */
    @Then("^I impersonate as (.+) with name (.+)$")
    public void impersonate(String userType, String username) throws InterruptedException {
        System.out.println("Impersonating as " + userType + " with username " + username);
        impersonatePage.impersonateUser(username);
        System.out.println("Impersonated user.");
    }

    @Then("^I impersonate back as the Program Manager$")
    public void impersonateProgramManager() throws InterruptedException {
        System.out.println("Impersonating as program manager.");
        impersonatePage.impersonateAsProgram();
        System.out.println("Program manager impersonated.");
    }
}