package Steps;

import Base.BasePage;
import Pages.ExpensePage;
import Pages.TimesheetPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class TimesheetSteps {

    TimesheetPage timesheetPage = new TimesheetPage(BasePage.driver);
    ExpensePage expensePage = new ExpensePage(BasePage.driver);

    @Then("^I select (.+) as (.+) and continue$")
    public void selectContractorAndContinue(String value, String type) throws InterruptedException {
        System.out.println("Finding " +type+ "with details.");
        if (type.equals("Contractor")) {
            timesheetPage.selectContractor(value);
        }
        if (type.equals("Month & Week")) {
            timesheetPage.selectTimesheetDate(value);
        }
        System.out.println("Found " + type);
    }

    @Then("^I set in time as (.+) and out time as (.+) for week$")
    public void setInAndOutTimes(String entryTime, String outTime) throws InterruptedException {
        System.out.println("Setting in and out time for week..");
        timesheetPage.setInAndOutTIme(entryTime, outTime);
        System.out.println("Timesheet set and saved for the week");
    }

    @And("^I'm selecting (.+) with name (.+) and status (.+)$")
    public void selectContractorWithStatus(String type, String name, String status) throws InterruptedException {
        System.out.println("Selecting contractor with name: " + name + " and status as " + status);
        if (type.equals("Contractor") && status.equals("Pending")) {
            timesheetPage.hmSelectApproveTimesheet(name, status);
        }
        if (type.equals("Expense") && status.equals("Pending")) {
            expensePage.hmSelectApproveExpense(name, status);
        }
        System.out.println(name + " contractor selected");
    }

    @And("^I check total hours is (.+)$")
    public void totalFilledHours(String hours){
        Assert.assertEquals(hours, timesheetPage.totalFilledHours(hours));
        System.out.println("Total hours found as: " + hours );
    }

    @And("^I check Total Hours is (.+) and Amount is (.+)$")
    public void checkHoursAndAmount(String hours, String amount) throws InterruptedException {
        Assert.assertEquals(hours, timesheetPage.totalFilledHoursForTImesheet(hours));
        Assert.assertEquals(hours, timesheetPage.totalFilledBillForTImesheet(amount));
        System.out.println("Total hours found as: " + hours+ " and bill found : " +amount );
    }

    @And("^I check timesheet approving manager is (.+)$")
    public void checkTimesheetApprover(String approverName) throws InterruptedException {
        Assert.assertEquals(approverName, timesheetPage.timesheetApproverName(approverName));
        System.out.println("Approver name found as : " +approverName );
    }
}