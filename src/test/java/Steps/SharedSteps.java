package Steps;

import Base.BasePage;
import Pages.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class SharedSteps {
    WebDriver driver = BasePage.driver;
    AddJobPage addJobPage = new AddJobPage(driver);
    DashBoardPage dashBoardPage = new DashBoardPage(driver);
    LoginPage loginPage = new LoginPage(driver);
    InterviewApprovalPage interviewApprovalPage = new InterviewApprovalPage(driver);
    WorkOrderCreatePage workOrderCreatePage = new WorkOrderCreatePage(driver);

    @Then("^I'm waiting for (.+) (.+)$")
        public void waitFor(Long count, String type) throws InterruptedException {
        System.out.println("waiting for " + count + " " + type);
        if (type.equalsIgnoreCase("minutes")){
            TimeUnit.MINUTES.sleep(count);
        }
        if (type.equalsIgnoreCase("seconds")){
            TimeUnit.SECONDS.sleep(count);
        }
    }

    @And("^I select the created job with name (.+)$")
    public void selectJob(String jobName) throws InterruptedException {
        System.out.println("Selecting job with name: " + jobName);
        addJobPage.openJobIdDetails(jobName);
        System.out.println(jobName + " selected");
    }

    @And("^I select the (.+) job with status (.+)$")
    public void selectJobWithStatus(String jobType, String status) throws InterruptedException {
        System.out.println("Selecting job with status: " + status);
        if (jobType.equals("created")) {
            addJobPage.openJobStatus(status);
        }
        if (jobType.equals("approval")){
            addJobPage.openHmJobStatus(status);
        }
        else {
            System.out.println("Status not found..." + status);
        }
        System.out.println(status + " selected");
    }

    @And("^I check (.+) alert is shown with message (.+)$")
    public void alert(String alertType, String message){
        if (alertType.equals("success")) {
            Assert.assertEquals(message, addJobPage.successAlertText(message));
        }
        if (alertType.equals("danger")) {
            Assert.assertEquals(message, addJobPage.dangerAlertText(message));
        }
        System.out.println(alertType + " alert shown with message as: " + message );
    }

    @Then("^I'm on the (.+) page$")
    public void checkPage(String pageName) throws InterruptedException {
        System.out.println("Checking page name.");
            Assert.assertEquals(pageName, driver.getTitle());
        System.out.println("Page showing as: " + pageName);
    }

    @And("^Job (.+) is created$")
        public void createJoboffer(String type) throws InterruptedException {
        System.out.println("Creating " +type);
        if (type.equals("Offer")) {
            interviewApprovalPage.createOfferAsPMO();
        }
        if (type.equals("Workorder")){
            workOrderCreatePage.clickOnPmoCreateAndReleaseWorkorder();
        }
        System.out.println(type +" created");
    }

    @And("^I log out$")
    public void logout() throws InterruptedException {
        System.out.println("Logging out of the website.");
        loginPage.logout();
        System.out.println("Logged out.");
        }
    }