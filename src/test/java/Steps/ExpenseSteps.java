package Steps;

import Base.BasePage;
import Pages.ExpensePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ExpenseSteps {

    ExpensePage expensePage = new ExpensePage(BasePage.driver);

    @Then("^I select location as (.+) and contractor as (.+)$")
    public void selectlocationAndContrator(String location, String name) throws InterruptedException {
        System.out.println("Selecting location and contractor");
            expensePage.setLocationAndContractor(location, name);
        System.out.println("Selected location and contractor and continued..");
    }

    @And("^I add expense for (.+) with type (.+) for amount (.+)$")
    public void setExpense(String day, String type, String amount) {
        System.out.println("Adding expense with details");
            expensePage.setAddExpense(day, type, amount);
        System.out.println("added expense");
    }
}