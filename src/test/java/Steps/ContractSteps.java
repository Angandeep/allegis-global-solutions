package Steps;

import Base.BasePage;
import Pages.ContractsPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class ContractSteps {

    ContractsPage contractsPage = new ContractsPage(BasePage.driver);

    @Then("^I select update reason as (.+) with (.+) as (.+)$")
    public void updateContracts(String updateReason, String uReason, String reason) throws InterruptedException {
        System.out.println("Updating contracts with specifications provided.");
        contractsPage.updateContractReason(updateReason);
        if (updateReason.equals("Extension")) {
            contractsPage.extendContract(reason);
        }
        if (updateReason.equals("Rate Change")) {
            contractsPage.effectiveDateRateChange(reason);
        }
        if (updateReason.equals("Non Financial Change")) {
            contractsPage.addRequestNotes(reason);
        }
        System.out.println("Contract updated.....");
    }

    @And("^I select contractor with name (.+) and status (.+)$")
    public void selectJobWithStatus(String name, String status) throws InterruptedException {
        System.out.println("Selecting contractor with name: " + name + " and status as " + status);
        if (status.equals("Pending")) {
            contractsPage.selectHmContractorWithStatus(name, status);
        }
        if (status.equals("Supplier Pending")) {
            contractsPage.vendorSelectContractorWithStatus(name, status);
        }
        System.out.println(name + " contractor selected");
    }

    @And("^I select contractor for extension with name (.+) and status (.+)$")
    public void selectContractorWithStatus(String name, String status) throws InterruptedException {
        System.out.println("Selecting contractor with name: " + name + " and status as " + status);
        if (status.equals("Pending")) {
            contractsPage.hmSelectContractorWithStatus(name, status);
        }
        System.out.println(name + " contractor selected");
    }

    @Then("^I check that contract status is (.+)$")
    public void checkContractStatus(String status) {
        System.out.println("Checking contract status is ..." + status);
        Assert.assertEquals(status, contractsPage.checkVpContractStatus());
        System.out.println("Status verified as: " + status);
    }

    @And("^I select the (.+) Contract with name (.+)$")
    public void selectContractWithStatus(String status, String jobName) throws InterruptedException {
        System.out.println("Selecting Contract with name: " + jobName + " and status as " + status);
        if (status.equals("Approved")) {
            contractsPage.selectPmContractJob(status, jobName);
        }
        System.out.println(jobName + " selected");
    }
}