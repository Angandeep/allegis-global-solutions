package Steps;

import Base.BasePage;
import Pages.OfferApprovalPage;
import Pages.TimesheetPage;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonAnyFormatVisitor;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class OfferApprovalSteps {

    OfferApprovalPage offerApprovalPage = new OfferApprovalPage(BasePage.driver);
    TimesheetPage timesheetPage = new TimesheetPage(BasePage.driver);

    @Then("^I check that Action status is (.+)$")
    public void checkOfferActionStatus(String status){
        System.out.println("Checking action status is ..." + status);
            Assert.assertEquals(status, offerApprovalPage.checkActionStatus());
        System.out.println("Status verified as: " + status);
    }

    @Then("^I (.+) the offer$")
    public void takeOfferAction(String action){
        System.out.println(action+ "ing offer");
        if (action.equals("Approve")){
            offerApprovalPage.approveJobOffer();
        }
        if (action.equals("Accept")){
            offerApprovalPage.vendorAcceptOffer();
        }
        if (action.equals("Reject")){
            offerApprovalPage.rejectJobOffer();
        }
        if (action.equals("Reject Offer")){
            offerApprovalPage.vendorRejectedOffer();
        }
        if (action.equals("reject")){
            offerApprovalPage.rejectExtension();
        }
        System.out.println("offer " + action + "ed");
    }

    @Then("^I check that offer status is (.+)$")
    public void checkOfferStatus(String status) {
        System.out.println("Checking offer status is ..." + status);
        if (status.equals("PMO - Review")) {
            Assert.assertEquals(status, offerApprovalPage.checkOfferStatus());
        }
        if (status.equals("Approved")){
            Assert.assertEquals(status, offerApprovalPage.checkVendorApprovedStatus());
        }
        if (status.equals("Pending Approval - HM")){
            Assert.assertEquals(status, offerApprovalPage.checkVendorApprovedStatus());
        }
        if (status.equals("Pending Approval - HM")){
            Assert.assertEquals(status, offerApprovalPage.checkVendorApprovedStatus());
        }
        if (status.equals("Rejected")){
            Assert.assertEquals(status, offerApprovalPage.checkPmoApprovedStatus());
        }
        System.out.println("Status verified as: " + status);
    }

    @Then("^I give (.+) reason as (.+) and notes as (.+)$")
    public void setRejectionReason(String type, String option, String note) throws InterruptedException {
        System.out.println("Updating rejection reason as " + option + "and comments as " + note);
        if (type.equals("rejection")) {
            offerApprovalPage.setOfferRejectionReason(option, note);
        }
        if (type.equals("Reject")){
            timesheetPage.setTimesheetRejectionReason(option, note);
        }
        System.out.println("Updated job as " + option + "and comments as " + note);
    }
}