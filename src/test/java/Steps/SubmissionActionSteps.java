package Steps;

import Base.BasePage;
import Pages.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class SubmissionActionSteps {

    SubmissionActionPage submissionActionPage = new SubmissionActionPage(BasePage.driver);
    ScheduleInterviewPage scheduleInterviewPage = new ScheduleInterviewPage(BasePage.driver);
    InterviewApprovalPage InterviewApprovalPage = new InterviewApprovalPage(BasePage.driver);
    OfferApprovalPage offerApprovalPage = new OfferApprovalPage(BasePage.driver);
    WorkOrderCreatePage workOrderCreatePage = new WorkOrderCreatePage(BasePage.driver);
    ContractsPage contractsPage = new ContractsPage(BasePage.driver);
    TimesheetPage timesheetPage = new TimesheetPage(BasePage.driver);
    ExpensePage expensePage = new ExpensePage(BasePage.driver);

    @Then("^I check that submission status is (.+)$")
        public void checkSubmissionStatus(String status){
        System.out.println("Checking submission status is ..." + status);
        if (status.equals("Submission")) {
            Assert.assertEquals(status, submissionActionPage.checkSubmissionStatus());
        }
        if (status.equals("MSP Reviewed")){
            Assert.assertEquals(status, submissionActionPage.checkStatus());
        }
        if (status.equals("Shortlisted")){
            Assert.assertEquals(status, submissionActionPage.checkShortlisted());
        }
        if (status.equals("Client Review")){
            Assert.assertEquals(status, submissionActionPage.checkClientReviewStatus());
        }
        if (status.equals("Waiting for Approval")){
            Assert.assertEquals(status, scheduleInterviewPage.checkStatusAsWaitingForApproval());
        }
        System.out.println("Status verified as: " + status);
    }

    @Then("^I'm clicking on (.+)$")
        public void clickOnItem(String option){
        System.out.println("Clicking on " + option);
        if (option.equals("Action")){
            submissionActionPage.clickOnActionIcon();
        }
        if (option.equals("Shortlist")){
            submissionActionPage.shortListResume();
        }
        if (option.equals("Schedule Interview")){
            submissionActionPage.scheduleInterview();
        }
        if (option.equals("Interview Completed")){
            InterviewApprovalPage.clickOnInterviewComplete();
        }
        if (option.equals("Create Offer")){
            InterviewApprovalPage.clickOnInterviewComplete();
        }
        if (option.equals("Release Offer")){
            offerApprovalPage.clickOnPmoReleaseOffer();
        }
        if (option.equals("Create Workorder")){
            workOrderCreatePage.clickOnPmoCreateWorkorder();
        }
        if (option.equals("Accept Workorder")){
            workOrderCreatePage.clickOnAcceptWorkorder();
        }
        if (option.equals("Onboard")){
            workOrderCreatePage.clickOnboard();
        }
        if (option.equals("Background Check Reviewed")){
            workOrderCreatePage.backGroundVerified();
        }
        if (option.equals("Update Contract")){
            contractsPage.clickOnUpdateContract();
        }
        if (option.equals("Submit Timesheet")){
            timesheetPage.setSubmitTimesheet();
        }
        if (option.equals("Approve Timesheet")){
            timesheetPage.setHmApproveTimesheet();
        }
        if (option.equals("Submit For Approval")){
            expensePage.setSubmitForApproval();
        }
        if (option.equals("Approve Expense")){
            expensePage.setApproveExpense();
        }
        if (option.equals("Add Timesheet")){
            expensePage.setApproveExpense();
        }
        if (option.equals("Reject Candidate")){
            submissionActionPage.rejectCandidate();
        }
        if (option.equals("reject candidate")){
            submissionActionPage.pmoRejectCandidate();
        }
        if (option.equals("Reject Timesheet")){
            timesheetPage.setHmRejectTimesheet();
        }
        if (option.equals("Reject Expense")){
            expensePage.setRejectExpense();
        }
        else {
            System.out.println("Option not available");
        }
    }

    @And("^I select the reviewing resume with name (.+)$")
    public void selectReviewJob(String jobName) throws InterruptedException {
        System.out.println("Selecting job with name: " + jobName);
        submissionActionPage.openReviewJobIdDetails(jobName);
        System.out.println(jobName + " selected");
    }

    @And("^I select the shortlisted resume with name (.+)$")
    public void selectShortlistedUser(String jobName) throws InterruptedException {
        System.out.println("Selecting user with name: " + jobName);
        submissionActionPage.openShortlistedJobDetails(jobName);
        System.out.println(jobName + " selected");
        }

    @Then("^I select reason for (.+) as (.+) and comments as (.+)$")
    public void setRejectReason(String type, String option, String note) throws InterruptedException {
        System.out.println("Updating rejection reason as " + option + " and comments as " + note);
        if (type.equals("rejection")) {
            submissionActionPage.setResumeReject(option, note);
        }
        if (type.equals("Rejection")){
            submissionActionPage.setInterviewReject(option, note);
        }
        if (type.equals("Reject")){
            submissionActionPage.setOfferReject(option, note);
        }
        if (type.equals("reject")){
            offerApprovalPage.setExtensionRejectionReason(option, note);
        }
        System.out.println("Updated job as " + option + " and comments as " + note);
    }

}