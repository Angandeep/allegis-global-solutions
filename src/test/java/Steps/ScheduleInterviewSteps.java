package Steps;

import Base.BasePage;
import Pages.ScheduleInterviewPage;
import cucumber.api.java.en.And;

public class ScheduleInterviewSteps {

    ScheduleInterviewPage scheduleInterviewPage = new ScheduleInterviewPage(BasePage.driver);

    @And("^I schedule Interview with duration (.+) and type (.+)$")
        public void schedulingInterview(String time, String type) throws InterruptedException {
        System.out.println("Filling up the duration and type of the interview....");
        scheduleInterviewPage.scheduleInterview(time, type);
        System.out.println("Interview form filled....");
    }

    @And("^I select slot as (.+)$")
         public void selectInterviewSlot(String slot){
        System.out.println("Selecting slot...");
        scheduleInterviewPage.selectInterviewSlot(slot);
        System.out.println("Slot selected");
            }
    }