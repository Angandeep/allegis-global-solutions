package Steps;

import Base.BasePage;
import Pages.LoginPage;
import Pages.OfferApprovalPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class LoginSteps{
    LoginPage loginPage = new LoginPage(BasePage.driver);
    OfferApprovalPage offerApprovalPage = new OfferApprovalPage(BasePage.driver);

    /*
    Login to the application
    */
    @Given("^I login with username (.+) and password (.+)$")
    public void SignIn(String username, String password) {
        System.out.println("Entering username and password as " + username + " and " + password);
        loginPage.login(username, password);
        System.out.println("Entered username and password and logged in to AGS application.");
    }

    @Given("^I login as (.+)$")
    public void SignInAs(String userType) throws InterruptedException {
        System.out.println("Logging in as: " + userType);
        if (userType.equals("Program Manager")){
            loginPage.loginAsPMO();
        }
        if (userType.equals("Contractor")){
            offerApprovalPage.loginAsContractor();
        }
    }

    @And("^I change password to (.+)$")
    public void changePassword(String newPassword){
        System.out.println("Changing password");
        loginPage.contractorChangePassword(newPassword);
        System.out.println("Changed password");
    }
}