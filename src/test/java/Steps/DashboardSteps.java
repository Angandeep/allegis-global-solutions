package Steps;

import Base.BasePage;
import Pages.DashBoardPage;
import cucumber.api.java.en.Then;

public class DashboardSteps {
    DashBoardPage dashBoardPage = new DashBoardPage(BasePage.driver);

    @Then("^I click on (.+) in dashboard$")
    public void selectFromDashboard(String itemName) throws InterruptedException {
        System.out.println("Clicking on " + itemName);
        dashBoardPage.selectDashboardItem(itemName);
        System.out.println(itemName + " clicked");
    }
}