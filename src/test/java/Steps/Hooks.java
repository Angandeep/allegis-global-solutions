package Steps;

import Base.BasePage;
import Reporting.ExtentReportUI;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Hooks{
    private static ExtentReportUI extentReport;
    private static boolean isReporterRunning;
    BasePage basePage = new BasePage(BasePage.driver);
    WebDriver driver = basePage.driver();
    public static String reportLocation = "src\\test\\resources\\Reports\\";

    @Before
    /**
     * Open the browser and Chromedriver
     * Author: Angandeep
     */
    public void setUp() { basePage.setUp(); }

    @Before
    /**
     * Checks if extent-report is running
     * Author: Angandeep
     */
    public void beforeScenario(Scenario scenario){
        if (!isReporterRunning){
            extentReport = new ExtentReportUI(ExtentReportUI.fileName);
            isReporterRunning = true;
        }
    }

    @After
    /**
     * Adds screenshots to report for failed scenario
     * Author: Angandeep
     */
    public void afterScenario(Scenario scenario) throws IOException {
        String screenshotName = reportLocation + System.currentTimeMillis() + ".png";
        if(scenario.isFailed()) {
            captureScreenshot(scenario);
        }
        extentReport.CreateTest(scenario, screenshotName);
        extentReport.flushReports();
        basePage.tearDown();
    }

    /**
     * Captures screenshot
     * Author: Angandeep
     */
    private void captureScreenshot(Scenario scenario) throws IOException {
        try {
            File scrFile = ((TakesScreenshot)BasePage.driver).getScreenshotAs(OutputType.FILE);
            File destFile = new File(reportLocation + System.currentTimeMillis() + ".png");
            FileUtils.copyFile(scrFile, destFile);
            System.out.println("Screenshot saved.");
        }catch (Exception e){
            System.out.println("Not able to take screenshot " + Arrays.toString(e.getStackTrace()));
        }
    }
}