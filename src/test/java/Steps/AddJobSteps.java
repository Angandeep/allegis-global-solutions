package Steps;

import Base.BasePage;
import Pages.AddJobPage;
import Reporting.ExtentReportUI;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class AddJobSteps {
    AddJobPage addJobPage = new AddJobPage(BasePage.driver);
    ExtentReportUI reporter =null;

    @Then("^I add job with title (.+) for (.+) with Level as (.+)$")
        public void addJob(String jobTitle, String location, String level) throws InterruptedException {
        System.out.println("Adding job with specifications provided.");
            addJobPage.addJob(jobTitle, location, level);
        System.out.println("Moving to next page....");
    }

    @And("^I select pre-identified (.+) with name (.+) and Vendor (.+) and (.+) expense with (.+) Virtual Work$")
    public void durationAndDescription(String option, String name, String vendor, String expense, String work) throws InterruptedException {
       System.out.println("Filling up the duration and description of the job....");
        addJobPage.fillDurationDescription(option, name, vendor, expense, work);
       System.out.println("Job created....");
    }

    @And("^I select pre-identified (.+) with name (.+) and the Vendor as (.+)$")
    public void durationAndDescription(String option, String name, String vendor) throws InterruptedException {
        System.out.println("Filling up the duration and description of the job....");
        addJobPage.fillDurationDescription(option, name, vendor);
        System.out.println("Filled details as PMO....");
    }

    @And("^I select Hiring Manager as (.+) and (.+) Expense with (.+) Virtual Work$")
    public void durationAndDescriptionPmo(String hm, String expense, String work) throws InterruptedException {
        System.out.println("Filling up the hiring manager of the job....");
        addJobPage.fillPMOCustFields(hm, expense, work);
        System.out.println("Job created....");
    }
}