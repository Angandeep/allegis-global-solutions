package Base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class BasePage {
    public static WebDriver driver;
    public static WebDriverWait wait;
   public String PAGE_URL;
   public String PAGE_TITLE;
    public ExtentReports extentReports;
    public static ExtentTest scenaroDef;
    public static ExtentTest features;

    public static String reportLocation = "src\\test\\resources\\Reports\\";

   public BasePage(WebDriver driver){
       this.driver = driver;
       PageFactory.initElements(driver, this);
   }

    /**
     * Open the browser and Chromedriver
     * Author: Angandeep
     */
    public void setUp(){
        System.out.println("Opening browser for new scenario...");
        WebDriverManager.chromedriver().version("2.44").setup();
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

   public void loadPMOLoginPage(){
        driver.get("https://qa-ags.simplifyvms.com/program");
   }

   public String getPageUrl(){
       return PAGE_URL;
   }

    public String getPageTitle(){
        return PAGE_TITLE;
    }

   public void setElementText(WebElement element, String text){
       element.isDisplayed();
       element.clear();
       element.sendKeys(text);
       Assert.assertEquals(element.getAttribute("value"), text);
   }

   public void setMoveToElement(WebElement element){
       element.isDisplayed();
       Actions actions = new Actions(driver());
       actions.moveToElement(element).build().perform();
   }

   public void clickElement(WebElement element){
        waitForClickingElement(element);
        element.click();
   }

   public boolean isElementDisplayed(WebElement element){
        return element.isDisplayed();
   }

    public boolean isElementEnabled(WebElement element){
        return element.isEnabled();
    }

   public void selectValueInDropdown(WebElement dropdown, String value){
       waitForElementToBeVisible(dropdown);
       Select select = new Select(dropdown);
       select.selectByVisibleText(value);
   }

    public void selectIndexInDropdown(WebElement dropdown, Integer index){
        waitForElementToBeVisible(dropdown);
        Select select = new Select(dropdown);
        select.selectByIndex(index);
    }

    /**
     * Gets current day value
     * Author: Angandeep
     */
    public String getCurrentDay(){
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        String todayStr = Integer.toString(todayInt);
        return todayStr;
    }

    /**
     * Gets next day value
     * Author: Angandeep
     */
    public String futureDayTomorrow(){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        date = calendar.getTime();
        String tommorowsDate = new SimpleDateFormat("MM/dd/yyyy").format(date);
        return tommorowsDate;
    }

    public void selectDate(WebDriver driver, WebElement element, String dateVal){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].setAttribute('value','"+dateVal+"');",element);
    }

    public void selectCustomDate(WebElement dateBox, List<WebElement> elements, String custDate){
                clickElement(dateBox);
        List<WebElement> allDates=driver.findElements(By.xpath(String.valueOf(elements)));
        for(WebElement ele:allDates)
        {
            String date=ele.getText();
            if(date.equalsIgnoreCase(custDate))
            {
                ele.click();
                break;
            }
        }
    }

   public String getTextValue(WebElement element){
       waitForElementToBeVisible(element);
        return element.getText();
   }

   public WebDriver driver(){
       return driver;
   }

    public void waitForPageLoad(){
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    }

    /**
     * Closes browser and kills driver
     * Author: Angandeep
     */
   public void tearDown() {
       if (driver != null) {
           driver.quit();
           System.out.println("Closing browser..........Scenario end....................");
       }
   }

    /**
     * Moves to new Window
     * Author: Angandeep
     */
    public void goToNewWindow() throws InterruptedException{
        String parentWinHandle=driver.getWindowHandle();
        Set<String> winHandles=driver.getWindowHandles();
        for(String handle:winHandles){
            if(!handle.equals(parentWinHandle)){
                driver.switchTo().window(handle);
                waitForPageLoad();
                System.out.println("Switching to new window");
            }
        }
    }

    public void returnToParentWindow() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        System.out.println("Returning to parent window");
        String parentWinHandle = driver.getWindowHandle();
        driver.close();
        TimeUnit.MILLISECONDS.sleep(500);
        Set<String> winHandles = driver.getWindowHandles();
        for (String handle : winHandles) {
            if (!handle.equals(parentWinHandle)) {
                driver.switchTo().window(handle);
                System.out.println("Returned back to parent window");
            }
        }
    }

    public void waitForElementToBeVisible(WebElement element){
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForClickingElement(WebElement element){
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForElementToBeInvisible(WebElement element){
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public void waitForElementToSelect(WebElement element){
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeSelected(element));
    }
}