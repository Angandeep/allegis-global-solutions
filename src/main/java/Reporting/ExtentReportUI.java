package Reporting;

import Base.BasePage;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import cucumber.api.Scenario;
import gherkin.formatter.model.Result;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

import static gherkin.formatter.model.Result.FAILED;
import static gherkin.formatter.model.Result.PASSED;

public class ExtentReportUI extends BasePage {

    public static String fileName = reportLocation + "extent-report.html";
    private ExtentHtmlReporter htmlReporter;
    public ExtentReports extentReports;

    public ExtentReportUI(String fileName) {
        super(driver);
        htmlReporter = new ExtentHtmlReporter(new File(fileName));
        extentReports = new ExtentReports();
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle("Simplify VMS QA Report for AGS");
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName("AGS Testing report");
        extentReports.attachReporter(htmlReporter);

        extentReports.setSystemInfo("Operating System", System.getProperty("os.name"));
        extentReports.setSystemInfo("User Name", System.getProperty("user.name"));
    }

    /**
     * Creates Test and captures screenshot
     * Author: Angandeep
     */
    public void CreateTest(Scenario scenario, String ScreenshotFile) throws IOException {
        if(scenario !=null) {
            String testName = getScenarioTitle(scenario);
            switch (scenario.getStatus()) {
                case PASSED:
                    extentReports.createTest(testName).pass("Passed");
                    break;

                case FAILED:
                    String errorMsg = getErrorMessage(scenario);
                    ScreenshotFile = reportLocation + System.currentTimeMillis() + ".png";
                    extentReports.createTest(testName).fail(errorMsg).addScreenCaptureFromPath(ScreenshotFile);
                    break;

                default:
                    extentReports.createTest(testName).skip("Skipped");
                    break;
            }
        }
    }

    /**
     * Gets error message from failed step
     * Author: Angandeep
     */
    private String getErrorMessage(Scenario scenario){
        List<Result> testResultsList = null;
        List<Result> failedStepList = null;

        try {
            Field stepResults = scenario.getClass().getDeclaredField("stepResults");
            stepResults.setAccessible(true);
            testResultsList =  ( List<Result>)stepResults.get(scenario);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (testResultsList !=null && !testResultsList.isEmpty()){
            failedStepList = testResultsList.stream().filter((x) ->{
                return x.getErrorMessage() !=null;
            }).collect(Collectors.toList());
        }
        if (failedStepList !=null && !failedStepList.isEmpty()){
            return failedStepList.get(0).getErrorMessage();
        }
        return "";
    }

    /**
     * Gets the scenario title
     * Author: Angandeep
     */
    private String getScenarioTitle(Scenario scenario){
        return scenario.getName();
    }

    /**
     * Flushes report
     * Author: Angandeep
     */
    public void flushReports(){
        if (extentReports !=null) {
            extentReports.flush();
        }
    }
}