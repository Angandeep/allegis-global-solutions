package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.concurrent.TimeUnit;

public class DashBoardPage extends BasePage {

    public DashBoardPage(WebDriver driver){
        super(driver);
    }

    @FindBy(how= How.XPATH, xpath = "//*[@id='pending_actions_widget']/div[1]/ul/li[1]/a/label")
    WebElement pendingReviewJob;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Jobs Pending Approval')]")
    WebElement pendingApprovalJob;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Jobs Pending Release')]")
    WebElement pendingReleaseJob;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'New Job Requests')]")
    WebElement newJobRequest;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[3]/nav/div/a/span")
    WebElement dashBoardName;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Resumes To Review')]")
    WebElement resumesToReview;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Pending Interviews')]")
    WebElement pendingInterviews;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Post-Interview Actions')]")
    WebElement postInterviewActions;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[2]/nav/ul/li[1]/a/i/img")
    WebElement dashBoardIconLeftMenu;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[2]/div[4]/nav/ul/li[1]/a/i/img")
    WebElement hmDashBoardIconLeftMenu;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Interview')]")
    WebElement interviewLeftMenu;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Interviews')]")
    WebElement interviewsMenuOption;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Offers Pending Approval')]")
    WebElement offersPendingApproval;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Approved Offers Pending PMO Release')]")
    WebElement offersPendingPMORelease;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Pending Offers')]")
    WebElement vendorPendingOffers;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Offers Pending Workorders Creation')]")
    WebElement pmoWorkorderPending;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Pending Workorders')]")
    WebElement vendorPendingWorkorder;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Workorders Pending Approval')]")
    WebElement hmPendingWorkorder;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Validate Onboarding Completion')]")
    WebElement pmoValidateOnboarding;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[2]/nav/ul/li[7]/a/span[2]")
    WebElement contractLeftMenu;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[2]/div[4]/nav/ul/li[7]/a/span[2]")
    WebElement hmContractLeftMenu;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Contracts (All)')]")
    WebElement contractsMenuOption;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Contract Amendment Workflows')]")
    WebElement contractsPendingWorkflow;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Contract Extensions Pending Approval')]")
    WebElement hmContractExtensionsPending;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Pending Contract Extension')]")
    WebElement vendorPendingContractExtension;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Contract Amendments Pending Approval')]")
    WebElement hmPendingContractAmendment;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[2]/nav/ul/li[9]/a/span[2]")
    WebElement timeSheetLeftMenu;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Add Timesheet')]")
    WebElement addTimesheetMenuOption;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Timesheets Pending Approval')]")
    WebElement hmPendingTimesheetApprove;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[2]/nav/ul/li[10]/a/span[2]")
    WebElement expenseLeftMenu;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Add Expense')]")
    WebElement addExpenseMenuOption;

    @FindBy(how= How.XPATH, xpath = "//*[contains(text(),'Expense Pending Approval')]")
    WebElement hmPendingExpenseApprove;


    public void selectDashboardItem(String ItemName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        if (ItemName.contains("Jobs Pending Review")) {
            clickElement(pendingReviewJob);
        }
        if (ItemName.contains("Jobs Pending Approval")) {
            clickElement(pendingApprovalJob);
        }
        if (ItemName.contains("Jobs Pending Release")) {
            clickElement(pendingReleaseJob);
        }
        if (ItemName.contains("New Job Requests")) {
            clickElement(newJobRequest);
        }
        if (ItemName.contains("Resumes To Review")) {
            clickElement(resumesToReview);
        }
        if (ItemName.equals("Pending Interviews")) {
            clickElement(pendingInterviews);
        }
        if (ItemName.contains("Post-Interview Actions")) {
            clickElement(postInterviewActions);
        }
        if (ItemName.equals("Interviews")) {
            selectInterviewsFromLeftMenu();
        }
        if (ItemName.equals("Offers Pending Approval")) {
            clickElement(offersPendingApproval);
        }
        if (ItemName.equals("Approved Offers Pending PMO Release")){
            clickElement(offersPendingPMORelease);
        }
        if (ItemName.equals("Pending Offers")){
            clickElement(vendorPendingOffers);
        }
        if (ItemName.equals("Offers Pending Workorders Creation")){
            clickElement(pmoWorkorderPending);
        }
        if (ItemName.equals("Pending Workorders")){
            clickElement(vendorPendingWorkorder);
        }
        if (ItemName.equals("Workorders Pending Approval")){
            clickElement(hmPendingWorkorder);
        }
        if (ItemName.equals("Validate Onboarding Completion")){
            clickElement(pmoValidateOnboarding);
        }
        if (ItemName.equals("Contracts")) {
            selectContractsFromLeftMenu();
        }
        if (ItemName.equals("Contract")) {
            selectHmContractsFromLeftMenu();
        }
        if (ItemName.equals("Contract Extensions Pending Approval")) {
            clickElement(hmContractExtensionsPending);
        }
        if (ItemName.equals("Pending Contract Extension")) {
            clickElement(vendorPendingContractExtension);
        }
        if (ItemName.equals("Contract Amendments Pending Approval")) {
            clickElement(hmPendingContractAmendment);
        }
        if (ItemName.equals("Add Timesheet")) {
            addTimesheetsFromLeftMenu();
        }
        if (ItemName.equals("Timesheets Pending Approval")) {
            clickElement(hmPendingTimesheetApprove);
        }
        if (ItemName.equals("Add Expense")) {
            addExpenseFromLeftMenu();
        }
        if (ItemName.equals("Expense Pending Approval")) {
            clickElement(hmPendingExpenseApprove);
        }
        if (ItemName.equals("Contract Amendment Workflows")) {
            selectHmContractsPendingWorkflows();
        }
        else {
            System.out.println("Matching option not found in Dashboard.");
        }
  }

    public void selectInterviewsFromLeftMenu(){
        setMoveToElement(dashBoardIconLeftMenu);
        clickElement(interviewLeftMenu);
        isElementDisplayed(interviewsMenuOption);
        clickElement(interviewsMenuOption);
    }

    public void selectContractsFromLeftMenu(){
            setMoveToElement(dashBoardIconLeftMenu);
            clickElement(contractLeftMenu);
            isElementDisplayed(contractsMenuOption);
            clickElement(contractsMenuOption);
    }

    public void selectHmContractsFromLeftMenu(){
        setMoveToElement(hmDashBoardIconLeftMenu);
        clickElement(hmContractLeftMenu);
        isElementDisplayed(contractsMenuOption);
        clickElement(contractsMenuOption);
    }

    public void selectHmContractsPendingWorkflows(){
        setMoveToElement(hmDashBoardIconLeftMenu);
        clickElement(hmContractLeftMenu);
        isElementDisplayed(contractsPendingWorkflow);
        clickElement(contractsPendingWorkflow);
    }

    public void addTimesheetsFromLeftMenu(){
        setMoveToElement(dashBoardIconLeftMenu);
        clickElement(timeSheetLeftMenu);
        isElementDisplayed(addTimesheetMenuOption);
        clickElement(addTimesheetMenuOption);
    }

    public void addExpenseFromLeftMenu(){
        setMoveToElement(dashBoardIconLeftMenu);
        clickElement(expenseLeftMenu);
        isElementDisplayed(addExpenseMenuOption);
        clickElement(addExpenseMenuOption);
    }
}