package Pages;

import Base.BasePage;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddJobPage extends BasePage {

    public AddJobPage(WebDriver driver) { super(driver);}

    @FindBy(how= How.XPATH, xpath = "//label[contains(text(),'Add Job')]")
    WebElement addJobBtn;

    @FindBy(how = How.XPATH, xpath = "//*[@class='wizard-title']")
    WebElement addJobPageTitle;

    @FindBy(how = How.XPATH, xpath = "//*[@id='select2-chosen-1']")
    WebElement jobTitleDropdown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen1_search']")
    WebElement jobTitleTextBox;

    @FindBy(how = How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement jobTitleResult;

    @FindBy(how = How.XPATH, xpath = "(//*[@class='select2-chosen'])[2]")
    WebElement jobLevelDropdown;

    @FindBy(how = How.XPATH, xpath = "/html/body/div[14]/div/input")
    WebElement jobLevelTextBox;

    @FindBy(how = How.XPATH, xpath = "/html/body/div[14]/ul/li[1]/div/span")
    WebElement jobLevelResult;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_location']")
    WebElement jobLocationDropdown;

    @FindBy(how = How.XPATH, xpath = "/html/body/div[15]/div/input")
    WebElement jobLocationTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@class='select2-match']")
    WebElement jobLocationResult;

    @FindBy(how = How.XPATH, xpath = "//input[@id='bill_rate']")
    WebElement billRateTextbox;

    @FindBy(how = How.XPATH, xpath = "//button[@class='btn btn-primary nextBtn pull-right next1step']")
    WebElement continueBtn;

    @FindBy(how = How.XPATH, xpath = "//h5[contains(text(),'Duration & Description')]")
    WebElement duration;

    @FindBy(how = How.XPATH, xpath = "//h5[contains(text(),'Custom Fields')]")
    WebElement customFields;

    @FindBy(how = How.XPATH, xpath = "//select[@id='pre_candidate']")
    WebElement identifiedDropDown;

    @FindBy(how = How.XPATH, xpath = "//input[@id='pre_name']")
    WebElement candidateNameTextBox;

    @FindBy(how = How.XPATH, xpath = "//input[@id='pre_supplier_name']")
    WebElement vendorNameTextBox;

    @FindBy(how = How.XPATH, xpath = "//input[@id='estimated_start_date']")
    WebElement startDate;

    @FindBy(how = How.XPATH, xpath = "//input[@id='estimated_end_date']")
    WebElement endDate;

    @FindBy(how = How.XPATH, xpath = "(//button[contains(text(),'Continue')])[2]")
    WebElement continued;

//    @FindBy(how = How.XPATH, xpath = "//button[@class=' btn btn-primary nextBtn pull-right gettimeestimation next2step']")
//    WebElement durationcontinueBtn;

    @FindBy(how = How.XPATH, xpath = "//div[@id='s2id_expenses_allowed']")
    WebElement expenseDropDown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen7_search']")
    WebElement expenseTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@class='select2-match']")
    WebElement expenseResult;

    @FindBy(how = How.XPATH, xpath = "//span[@id='select2-chosen-8']")
    WebElement virtualWork;

    @FindBy(how = How.XPATH, xpath = "//span[@id='select2-chosen-9']")
    WebElement PmoVirtualWork;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen8_search']")
    WebElement virtualWorkTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen9_search']")
    WebElement pmoVirtualWorkTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@class='select2-match']")
    WebElement virtualWorkResult;

    @FindBy(how = How.XPATH, xpath = "//input[@name='createJob']")
    WebElement submitJob;

    @FindBy(how = How.XPATH, xpath = "//button[@name='createJob']")
    WebElement createJobPmo;

    @FindBy(how = How.XPATH, xpath = "//div[@class='alert alert-success']")
    WebElement successAlert;

    @FindBy(how = How.XPATH, xpath = "//div[@class='alert alert-danger'][1]")
    WebElement dangerAlert;

    @FindBy(how = How.XPATH, xpath = "/html/body/div[2]/div[3]/div/div[1]/div/div/div/div/div/div/div[1]/strong/a")
    static WebElement jobDetailsInAlert;

    @FindBy(how= How.XPATH, xpath = "//a[@class='underline']")
    List<WebElement> jobIds;

    @FindBy(how= How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]")
    List<WebElement> jobStatus;

    @FindBy(how= How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td/span")
    List<WebElement> hmApproveJobStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='select2-chosen-4']")
    WebElement hmField;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen4_search']")
    WebElement hmFieldTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@class='select2-match']")
    WebElement hmFieldResult;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen8_search']")
    WebElement hmExpenseTextBox;

    public void clickOnAddJobBtn(){ clickElement(addJobBtn); }

   public void addJobTitle(String jobTitle) throws InterruptedException {
        waitForPageLoad();
       clickElement(jobTitleDropdown);
       setElementText(jobTitleTextBox,jobTitle);
       String resultText = getTextValue(jobTitleResult);
       if (resultText.contains(jobTitle)){
           clickElement(jobTitleResult);
           TimeUnit.MILLISECONDS.sleep(2000);
           System.out.println("Job selected as " + jobTitle);
       } else {
           System.out.println("Entered job not found....");
       }
   }

    public void addJobLevel(String jobLevel) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        clickElement(jobLevelDropdown);
        setElementText(jobLevelTextBox,jobLevel);
        String resultText = getTextValue(jobLevelResult);
        if (resultText.contains(jobLevel)){
            clickElement(jobLevelResult);
            System.out.println("Job level selected as " + jobLevel);
        } else {
            System.out.println("Entered job level not found....");
        }
    }

    public void addJobLocation(String jobLocation) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        clickElement(jobLocationDropdown);
        setElementText(jobLocationTextBox,jobLocation);
        String resultText = getTextValue(jobLocationResult);
        if (resultText.contains(jobLocation)){
            clickElement(jobLocationResult);
            System.out.println("Job location selected as " + jobLocation);
        } else {
            System.out.println("Entered job location not found....");
        }
    }

    public void billRateTextbox(){
        clickElement(billRateTextbox);
        String bill = getTextValue(billRateTextbox);
        if (bill.isEmpty()){
            billRateTextbox.sendKeys("50.00");
        }
        else{
            System.out.println("Bill is shown already as " + bill);
        }
    }

    public void Continue(){
        clickElement(continueBtn);
    }

    public void preIdentified(String option){
        selectValueInDropdown(identifiedDropDown, option);
    }

    public void setCandidateName(String candidateName) {
        setElementText(candidateNameTextBox, candidateName);
    }

    public void setVendorName(String vendorName){
        setElementText(vendorNameTextBox, vendorName);
    }

    public void StartDate(){
        String dateVal ="06/10/2020";
        selectDate(driver, startDate, dateVal);
    }

    public void EndDate(){
        String dateVal ="12/30/2021";
        selectDate(driver, endDate, dateVal);
    }

    public void continueAfterDuration(){
        clickElement(continued);
        System.out.println("Continued after filling duration details....");
    }

    public void expense(String option){
        clickElement(expenseDropDown);
        setElementText(expenseTextBox, option);
        String resultText = getTextValue(expenseResult);
        if (resultText.contains(option)){
            clickElement(expenseResult);
        } else {
            System.out.println("Entered expense option not found....");
        }
    }

    public void pmoJobexpense(String option){
        clickElement(expenseDropDown);
        setElementText(hmExpenseTextBox, option);
        String resultText = getTextValue(expenseResult);
        if (resultText.contains(option)){
            clickElement(expenseResult);
        } else {
            System.out.println("Entered expense option not found....");
        }
    }

    public void virtualWork(String option){
        clickElement(virtualWork);
        setElementText(virtualWorkTextBox, option);
        String resultText = getTextValue(virtualWorkResult);;
        if (resultText.contains(option)){
            clickElement(virtualWorkResult);
        } else {
            System.out.println("Entered virtual work option not found....");
        }
    }

    public void pmoVirtualWork(String option){
        clickElement(PmoVirtualWork);
        setElementText(pmoVirtualWorkTextBox, option);
        String resultText = getTextValue(virtualWorkResult);;
        if (resultText.contains(option)){
            clickElement(virtualWorkResult);
        } else {
            System.out.println("Entered virtual work option not found....");
        }
    }

    public void submitJob(){
        isElementEnabled(submitJob);
        clickElement(submitJob);
    }

    public String successAlertText(String text){
        waitForPageLoad();
        String message = getTextValue(successAlert);
        if (message.contains(text)){
            System.out.println("Alert message found.");
        }
        else{
            System.out.println("Message not found!");
        }
        return text;
    }

    public String dangerAlertText(String text){
        waitForPageLoad();
        String message = getTextValue(dangerAlert);
        if (message.contains(text)){
            System.out.println("Alert message found.");
        }
        else{
            System.out.println("Message not found!");
        }
        return text;
    }

    public void openJobIdDetails(String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        for (int i = 0; 1 < jobIds.size(); i++){
            if (getTextValue(jobIds.get(i)).contains(jobName)){
                clickElement(jobIds.get(i));
                break;
            }
        }
    }

    public void openJobStatus(String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        for (int i = 0; 1 < jobStatus.size(); i++){
            if (getTextValue(jobStatus.get(i)).contains(status)){
                clickElement(jobStatus.get(i));
                break;
            }
        }
    }

    public void openHmJobStatus(String status){
        for (int i = 0; 1 < hmApproveJobStatus.size(); i++){
            if (getTextValue(hmApproveJobStatus.get(i)).contains(status)){
                clickElement(hmApproveJobStatus.get(i));
                break;
            }
        }
    }

    public void fillHm(String hm){
        clickElement(hmField);
        setElementText(hmFieldTextBox, hm);
        String resultText = getTextValue(hmFieldResult);;
        if (resultText.contains(hm)){
            clickElement(hmFieldResult);
        } else {
            System.out.println("Entered Hiring Manager option not found....");
        }
    }

    public void addJob(String jobTitle, String location, String level) throws InterruptedException {
        waitForPageLoad();
        clickOnAddJobBtn();
        addJobTitle(jobTitle);
        addJobLevel(level);
        addJobLocation(location);
        billRateTextbox();
        Continue();
    }

    public void fillDurationDescription(String option, String name, String vendor){
        waitForPageLoad();
        preIdentified(option);
        if (option.equals("Yes")) {
            setCandidateName(name);
            setVendorName(vendor);
        }
        else
        {
            System.out.println("Pre-identified as No would not require name and vendor");
        }
        StartDate();
        EndDate();
        continueAfterDuration();
    }

    public void createPmoJob(){clickElement(createJobPmo);}

    public void fillPMOCustFields(String hm, String expense, String work) throws InterruptedException {
        fillHm(hm);
        TimeUnit.MILLISECONDS.sleep(1000);
        pmoJobexpense(expense);
        pmoVirtualWork(work);
        createPmoJob();
    }

    public void fillDurationDescription(String option, String name, String vendor, String expense, String work) throws InterruptedException {
        waitForPageLoad();
        preIdentified(option);
        if (option.equals("Yes")) {
            setCandidateName(name);
            setVendorName(vendor);
        }
        else
        {
            System.out.println("Pre-identified as No would not require name and vendor");
        }
        StartDate();
        EndDate();
        continueAfterDuration();
        expense(expense);
        virtualWork(work);
        submitJob();
    }
   }