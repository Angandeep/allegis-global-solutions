package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SignNDAPage extends BasePage {

    public SignNDAPage(WebDriver driver) {
        super(driver);
        this.PAGE_URL = "https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin";
    }

    @FindBy(how = How.XPATH, xpath = "//span[@class='bog']")
    List<WebElement> emailThreads;

    @FindBy(how = How.XPATH, xpath = "//*[@id='identifierId']")
    WebElement emailName;

    @FindBy(how = How.XPATH, xpath = "//*[@class='RveJvd snByac']")
    WebElement emailNext;

    @FindBy(how = How.XPATH, xpath = "//*[@name='password']")
    WebElement passwordText;

    @FindBy(how = How.XPATH, xpath = "//span[contains(text(),'Next')]")
    WebElement nextBtnPwd;

    @FindBy(how = How.XPATH, xpath = "//*[@id=':2d']")
    WebElement primaryMail;

    @FindBy(how = How.XPATH, xpath = "//*[@class='hP']")
    WebElement emailText;

    @FindBy(how = How.XPATH, xpath = "//a[contains(text(),'Review & Sign')]")
    WebElement reviewAndSign;

    @FindBy(how = How.XPATH, xpath = "//span[contains(text(),'OK')]")
    WebElement newWindowOkBtn;

    @FindBy(how = How.XPATH, xpath = "//div[contains(text(),'Click to sign')]")
    WebElement clickToSign;

    @FindBy(how = How.XPATH, xpath = "//span[contains(text(),'Type it in')]")
    WebElement typeSignature;

    @FindBy(how = How.XPATH, xpath = "//*[@id='typedSignatureInput']")
    WebElement enterSignature;

    @FindBy(how = How.XPATH, xpath = "//*[@id='insertButton']")
    WebElement insertBtn;

    @FindBy(how = How.XPATH, xpath = "//*[@id='signer-mobile-application']/div/div/div/div[1]/div[2]/div[1]/div/button/span")
    WebElement continueBtn;

    @FindBy(how = How.XPATH, xpath = "//*[@id='signer-mobile-application']/div/div/div/div[1]/div/div/button/span")
    WebElement iAgree;

    @FindBy(how = How.XPATH, xpath = "/html/body/div/div/div/p")
    WebElement acknowledgement;

    @FindBy(how = How.XPATH, xpath = "//*/div[1]/div[2]/div[2]/div[2]/div[2]/p[4]/text()[2]")
    WebElement sentPassword;

    public void openGmailPage() {
        driver.get(PAGE_URL);
    }

    public void EnterGmailUser(String user) {
        setElementText(emailName, user);
        clickElement(emailNext);
    }

    public void EnterGmailPwd(String pwd) {
        setElementText(passwordText, pwd);
        clickElement(nextBtnPwd);
    }

    public void loginToGmail(){
        openGmailPage();
        EnterGmailUser("abc.xyz.simplifyvms@gmail.com");
        EnterGmailPwd("Test@123");
        waitForPageLoad();
    }

    public void OpeningEmail(String subject) {
        for (int i = 0; i < emailThreads.size(); i++) {
            if (getTextValue(emailThreads.get(i)).contains(subject)) {
                clickElement(emailThreads.get(i));
                break;
            }
        }
    }

    public String getSentPassword(){
        String password = getTextValue(sentPassword);
        String pwd = password.replace("Password: ", "");
        return pwd;
    }

    public void signNDA() throws InterruptedException {
        clickReviewAndSign();
        clickOkBtn();
        setClickToSign();
        clickInsert();
        clickContinue();
        clickAgreeBtn();
    }

    public void clickReviewAndSign() throws InterruptedException {
        clickElement(reviewAndSign);
        goToNewWindow();
    }
    public void clickOkBtn() {
        clickElement(newWindowOkBtn);
    }

    public void setClickToSign() throws InterruptedException {
        clickElement(clickToSign);
        TimeUnit.MILLISECONDS.sleep(2000);
        clickElement(clickToSign);
        clickElement(typeSignature);
        setElementText(enterSignature, "abc");
    }

    public void clickInsert() throws InterruptedException {
        clickElement(insertBtn);
        TimeUnit.MILLISECONDS.sleep(3000);
    }

    public void clickContinue(){clickElement(continueBtn);}
    public void clickAgreeBtn(){clickElement(iAgree);}

    public String signAcknowledged(String text){
        waitForPageLoad();
        String message = getTextValue(acknowledgement);
        if (message.contains(text)){
            System.out.println("Acknowledged message found.");
        }
        else{
            System.out.println("Acknowledged message not found!");
        }
        return text;
    }
}