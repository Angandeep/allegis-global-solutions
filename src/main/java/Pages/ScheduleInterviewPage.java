package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ScheduleInterviewPage extends BasePage {

    public ScheduleInterviewPage(WebDriver driver) { super(driver); }
    AddJobPage addJobPage = new AddJobPage(driver);

    @FindBy(how = How.XPATH, xpath = "//*[@id='select2-chosen-1']")
    WebElement duration;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen1_search']")
    WebElement durationTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@class='select2-match']")
    WebElement durationResult;

    @FindBy(how = How.XPATH, xpath = "//*[@id='select2-chosen-3']")
    WebElement interviewType;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen3_search']")
    WebElement interviewTypeTextBox;

    @FindBy(how = How.XPATH, xpath = "//*[@class='select2-match']")
    WebElement interviewTypeResult;

    @FindBy(how = How.XPATH, xpath = "//*[@id='interview_start_date']")
    WebElement interviewStartDate;

    @FindBy(how = How.XPATH, xpath = "//*[@id='interview-btn']")
    WebElement continueBtn;

    @FindBy(how = How.XPATH, xpath = "//textarea[@name='interViewModel[phone_notes]']")
    WebElement phoneNotes;

    @FindBy(how = How.XPATH, xpath = "//textarea[@name='interViewModel[meeting_info]']")
    WebElement virtualNotes;

    @FindBy(how = How.XPATH, xpath = "//*[@class='not-available']")
    List<WebElement> interviewTime;

    @FindBy(how = How.XPATH, xpath = "//*[@class='btn btn-success']")
    WebElement sendInvite;

    @FindBy(how = How.XPATH, xpath = "//*[@class='tag label-interview-pending']")
    WebElement interviewStatus;

    public void interviewDuration(String time) throws InterruptedException {
        clickElement(duration);
        setElementText(durationTextBox, time);
        String resultText = getTextValue(durationResult);
        if (resultText.contains(time)){
            clickElement(durationResult);
            TimeUnit.MILLISECONDS.sleep(2000);
            System.out.println("Interview duration selected as " + time);
        } else {
            System.out.println("Entered Interview duration not found....");
        }
    }

    public void interviewType(String type) throws InterruptedException {
        clickElement(interviewType);
        setElementText(interviewTypeTextBox, type);
        String resultText = getTextValue(interviewTypeResult);
        if (resultText.contains(type)){
            clickElement(interviewTypeResult);
            TimeUnit.MILLISECONDS.sleep(2000);
            System.out.println("Interview type selected as " + type);
            if (type.contains("Phone Interview")){
                setElementText(phoneNotes, "abc");
            }
            if (type.contains("Virtual Interview")){
                setElementText(virtualNotes, "abc");
            }
        } else {
            System.out.println("Entered Interview type not found....");
        }
    }

    public void StartDate(){
        String dateVal = "06/10/2020";
        selectDate(driver, interviewStartDate, dateVal);
    }

    public void Continue(){ clickElement(continueBtn); }

    public void scheduleInterview(String time, String type) throws InterruptedException {
        interviewDuration(time);
        interviewType(type);
        StartDate();
        Continue();
        TimeUnit.MILLISECONDS.sleep(1000);
    }

    public void selectInterviewSlot(String slot){
        for (int i = 0; 1 < interviewTime.size(); i++) {
            if (getTextValue(interviewTime.get(i)).contains(slot)) {
                clickElement(interviewTime.get(i));
                break;
            }
        }
        clickElement(sendInvite);
    }

    public String checkStatusAsWaitingForApproval(){ return getTextValue(interviewStatus);}
}