package Pages;

import Base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SubmissionFormPage extends BasePage {
    AddJobPage addJobPage = new AddJobPage(BasePage.driver);

    public SubmissionFormPage(WebDriver driver) { super(driver);}

    @FindBy(how= How.NAME, name = "Candidates[first_name]")
    WebElement candidateFirstName;

    @FindBy(how= How.XPATH, xpath = "//h4[@class='m-b-10']")
    WebElement addSubmissionText;

    @FindBy(how= How.NAME, name = "Candidates[last_name]")
    WebElement candidateLastName;

    @FindBy(how= How.NAME, name = "VendorJobSubmission[estimate_start_date]")
    WebElement availableDate;

    @FindBy(how= How.NAME, name = "Candidates[dob]")
    WebElement candidateDob;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[11]/div[1]/div[2]/table/tbody/tr[5]/td[2]")
    WebElement selectAvailableDate;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[10]/div[1]/div[2]/table/tbody/tr[5]/td[2]")
    WebElement selectDob;

    @FindBy(how= How.XPATH, xpath = "//select[@name='Candidates[candidate_source_type]']")
    WebElement candidateSourceType;

    @FindBy(how= How.XPATH, xpath = "//select[@name='Candidates[new_candidate_type]']")
    WebElement candidateType;

    @FindBy(how= How.XPATH, xpath = "//select[@name='Candidates[remote_contractor]']")
    WebElement virtualContractorType;

    @FindBy(how= How.XPATH, xpath = "//select[@name='Candidates[shore_type]']")
    WebElement contractorShoreType;

    @FindBy(how= How.XPATH, xpath = "//select[@name='Candidates[ot_exempt_position]']")
    WebElement OtExemptionType;

    @FindBy(how= How.XPATH, xpath = "//select[@name='Candidates[is_legally_authorized]']")
    WebElement candidateAuthorized;

    @FindBy(how= How.XPATH, xpath = "(//*[@id='input'])[1]")
    WebElement candidateExperience;

    @FindBy(how= How.XPATH, xpath = "(//*[@id='input'])[2]")
    WebElement candidateRating;

    @FindBy(how= How.XPATH, xpath = "//*[@id='Candidates_resume']")
    WebElement uploadResume;

    @FindBy(how= How.XPATH, xpath = "//*[@id='VendorJobSubmission_vendor_bill_rate']")
    WebElement billRate;

    @FindBy(how= How.XPATH, xpath = "//input[@type='submit']")
    WebElement submitBtn;

    @FindBy(how= How.XPATH, xpath = "//input[@id='save_candidate']")
    WebElement submitCandidate;


    public void firstName() { setElementText(candidateFirstName,"DEF"); }
    public void lastName() { setElementText(candidateLastName,"UVW"); }

    public void setAvailableDate(){
        clickElement(availableDate);
        List<WebElement> allDates=driver.findElements(By.xpath("(//table[@class='table-condensed'])[1]//td"));
        for(WebElement ele:allDates)
        {
            String date=ele.getText();
            if(date.equalsIgnoreCase(addJobPage.getCurrentDay()))
            {
                ele.click();
                break;
            }
        }
    }

    public void setCandidateDob(){
        clickElement(candidateDob);
        List<WebElement> allDates=driver.findElements(By.xpath("/html/body/div[10]/div[1]/div[2]/table/tbody/tr/td"));
        for(WebElement ele:allDates)
        {
            String date=ele.getText();
            if(date.equalsIgnoreCase(addJobPage.getCurrentDay()))
            {
                ele.click();
                break;
            }
        }
    }

    public void setCandidateSourceType(){ selectIndexInDropdown(candidateSourceType, 1); }
    public void setCandidateType(){ selectIndexInDropdown(candidateType, 1); }
    public void setVirtualContractorType(){ selectIndexInDropdown(virtualContractorType, 1); }
    public void setContractorShoreType(){ selectIndexInDropdown(contractorShoreType, 1);}
    public void setOtExemptionType(){ selectIndexInDropdown(OtExemptionType, 1);}
    public void setEligibilityType(){ selectIndexInDropdown(candidateAuthorized, 1);}
    public void setExperienceType() { selectIndexInDropdown(candidateExperience, 1);}
    public void setCandidateRatingType(){ selectIndexInDropdown(candidateRating, 1);}

    public void uploadResume(){
        String path = System.getProperty("user.dir");
        uploadResume.sendKeys(path + "\\src\\test\\resources\\Properties\\AUTOMATION CODE STRUCTURE.pdf");
    }

    public void setBillRate(){
        billRate.click();
        billRate.clear();
        billRate.sendKeys("20.00");
    }

    public void submitForm() throws InterruptedException {
        clickElement(submitBtn);
        TimeUnit.MILLISECONDS.sleep(1000);
    }

    public void setSubmitCandidate(){ clickElement(submitCandidate); }

    public void fillSubmissionForm() throws InterruptedException {
        waitForPageLoad();
        firstName();
        lastName();
        setAvailableDate();
        setCandidateDob();
        setCandidateSourceType();
        setCandidateType();
        setVirtualContractorType();
        setContractorShoreType();
        setOtExemptionType();
        setEligibilityType();
        setExperienceType();
        setCandidateRatingType();
        uploadResume();
        setBillRate();
        submitForm();
        setSubmitCandidate();
    }
}