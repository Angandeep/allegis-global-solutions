package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class InterviewApprovalPage extends BasePage {

    public InterviewApprovalPage(WebDriver driver) { super(driver); }

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[5]")
    List<WebElement> interviewJobName;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> actionIcon;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[1]")
    List<WebElement> interviewStatus;

    @FindBy(how = How.XPATH, xpath = "(//*[@class='m-b-10'])[2]")
    List<WebElement> pmoInterviewStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[6]")
    List<WebElement> pmoInterviewJobName;

    @FindBy(how = How.XPATH, xpath = "//*[@name='interviewTiming']")
    WebElement interviewTiming;

    @FindBy(how = How.XPATH, xpath = "//*[@name='saveInterviewTiming']")
    WebElement saveInterviewTiming;

    @FindBy(how = How.XPATH, xpath = "//*[@class='tag label-approved']")
    WebElement interviewApproved;

    @FindBy(how = How.XPATH, xpath = "//span[@class='tag label-filled']")
    WebElement interviewCompleted;

    @FindBy(how= How.XPATH, xpath = "//li[@id='all']")
    WebElement allInterviews;

    @FindBy(how= How.XPATH, xpath = "//a[@class='btn btn-sm btn-success pull-right m-left']")
    WebElement interviewComplete;

    @FindBy(how= How.XPATH, xpath = "//select[@name='interview_completed_reason']")
    WebElement interviewCmpltReasonDropdwn;

    @FindBy(how= How.XPATH, xpath = "//*[@id='modal-interview-completed']/div/form/div/div[3]/button[1]")
    WebElement interviewCompleteSave;

    @FindBy(how= How.XPATH, xpath = "//*[@id='bill_rate']")
    WebElement billRateCreateOffer;

    @FindBy(how= How.XPATH, xpath = "//*[@id='offer_step_submit']")
    WebElement createOfferBtn;

    @FindBy(how= How.XPATH, xpath = "//a[@class='btn btn-sm btn-info pull-right m-left']")
    WebElement pmoScheduleInterview;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr")
    List<WebElement> tableRows;

    int rows = tableRows.size();

    public void openInterviewJobDetails(String jobName) throws InterruptedException {
        for (int j = 0; j < interviewJobName.size(); j++) {
            if (getTextValue(interviewJobName.get(j)).contains(jobName)) {
                clickElement(actionIcon.get(j));
                break;
            }
        }
    }

    public void acceptInterview(){
        clickElement(interviewTiming);
        clickElement(saveInterviewTiming);
    }

    public String checkInterviewApproved() {return getTextValue(interviewApproved);}

    public String checkInterviewCompleted(){
        return getTextValue(interviewCompleted);
    }

    public void goToAllInterview(){
        clickElement(allInterviews);
        waitForPageLoad();
    }

    public void openInterviewWithStatus(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++){
            if (getTextValue(interviewJobName.get(i)).contains(jobName) && getTextValue(interviewStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void clickOnInterviewComplete(){clickElement(interviewComplete);}
    public void interviewCompleted(String option){selectValueInDropdown(interviewCmpltReasonDropdwn, option);}
    public void interviewCompleteSave(){clickElement(interviewCompleteSave);}

    public void interviewComplete(String option){
        interviewCompleted(option);
        interviewCompleteSave();
    }

    public void selectPmoInterviewWithStatus(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++){
            if (getTextValue(pmoInterviewJobName.get(i)).contains(jobName) && getTextValue(pmoInterviewStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void fillBillRateCreateOffer(){ setElementText(billRateCreateOffer, "20.00");}
    public void clickBtnOfferCreate(){ clickElement(createOfferBtn);}
    public void pmoScheduleInterview() { clickElement(pmoScheduleInterview);}

    public void createOfferAsPMO(){
        fillBillRateCreateOffer();
        clickBtnOfferCreate();
    }

    public String checkJobStatus(String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
                for (int j = 0; j < pmoInterviewJobName.size(); j++) {
                    if (getTextValue(pmoInterviewJobName.get(j)).contains(jobName)) {
                        return getTextValue(pmoInterviewStatus.get(j));
                    }
                }
                return getTextValue(pmoInterviewStatus.get(0));
    }
}