package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class WorkOrderCreatePage extends BasePage {

    public WorkOrderCreatePage(WebDriver driver) { super(driver); }

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[11]")
    List<WebElement> ndaStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr")
    List<WebElement> tableRows;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[4]")
    List<WebElement> pmoOfferJobName;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> actionIcon;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm pull-right btn-success m-right']")
    WebElement pmoWorkorderCreateBtn;

    @FindBy(how = How.XPATH, xpath = "//button[@class='btn btn-success']")
    WebElement pmoWorkorderCreateAndReleaseBtn;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[1]")
    List<WebElement> hmWorkorderStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[5]")
    List<WebElement> hmWorkorderJobName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[6]")
    List<WebElement> pmoWorkorderJobName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]")
    List<WebElement> pmoWorkorderStatus;

    @FindBy(how = How.XPATH, xpath = "//a[@href='#workorder-workflow']")
    WebElement hmWorkorderWorkflowPage;

    @FindBy(how = How.XPATH, xpath = "//a[@href='#onboarding']")
    WebElement pmoOnboardingInfoPage;

    @FindBy(how = How.XPATH, xpath = "//a[@href='#back-ground-verification']")
    WebElement pmoBackgroundVerificationPage;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[7]")
    List<WebElement> vendorWorkorderJobName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[1]")
    List<WebElement> vendorWorkorderStatus;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> backgroundChecklist;

    @FindBy(how = How.XPATH, xpath = "//*[@class='table m-b-40 without-border table-white']/tbody/tr")
    List<WebElement> backgroundChecklistTable;

    @FindBy(how = How.XPATH, xpath = "//*[@id='bg_list_table']/div[1]/table/tbody/tr")
    List<WebElement> backgroundList;

    @FindBy(how = How.XPATH, xpath = "//*[@class='table m-b-40 without-border table-white']/tbody/tr/td[1]")
    List<WebElement> backgroundChecklistName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='bg_list_table']/div[1]/table/tbody/tr/td[3]")
    List<WebElement> backgroundStatus;

    @FindBy(how = How.XPATH, xpath = "(//select[@class='form-control'])[3]")
    WebElement bckgrndAttest;

    @FindBy(how = How.XPATH, xpath = "(//span[@class='btn btn-success'])[1]")
    WebElement saveBckgrndAttest;

    @FindBy(how = How.XPATH, xpath = "(//select[@class='form-control'])[4]")
    WebElement bckgrndCheck;

    @FindBy(how = How.XPATH, xpath = "(//span[@class='btn btn-success'])[2]")
    WebElement saveBckgrndCheck;

    @FindBy(how = How.XPATH, xpath = "(//select[@class='form-control'])[5]")
    WebElement bckgrndDrug;

    @FindBy(how = How.XPATH, xpath = "(//span[@class='btn btn-success'])[3]")
    WebElement saveBckgrndDrug;

    @FindBy(how = How.XPATH, xpath = "(//select[@class='form-control'])[6]")
    WebElement bckgrndReference;

    @FindBy(how = How.XPATH, xpath = "(//span[@class='btn btn-success'])[4]")
    WebElement saveBckgrndReference;

    @FindBy(how = How.XPATH, xpath = "(//select[@class='form-control'])[7]")
    WebElement bckgrndPrivacy;

    @FindBy(how = How.XPATH, xpath = "(//span[@class='btn btn-success'])[5]")
    WebElement saveBckgrndPrivacy;

    @FindBy(how = How.XPATH, xpath = "(//select[@class='form-control'])[8]")
    WebElement bckgrndSecurity;

    @FindBy(how = How.XPATH, xpath = "(//span[@class='btn btn-success'])[6]")
    WebElement saveBckgrndSecurity;

    @FindBy(how = How.XPATH, xpath = "(//button[@class='btn btn-success'])[1]")
    WebElement saveFullBackground;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm btn-success']")
    WebElement acceptWorkorder;

    @FindBy(how = How.XPATH, xpath = "//*[@id='type_of_timesheet']")
    WebElement timesheetTypeDrpDwn;

    @FindBy(how = How.XPATH, xpath = "//button[contains(text(),'Onboard')]")
    WebElement onboardBtn;

    @FindBy(how = How.XPATH, xpath = "//input[@id='checkboxforAll']")
    WebElement pmoBackgroundVerifyCheckbox;

    @FindBy(how = How.XPATH, xpath = "//button[contains(text(),'Background Check Reviewed')]")
    WebElement pmoBackgroundVerifyBtn;

    @FindBy(how = How.XPATH, xpath = "//a[contains(text(),' Back to List of Workorders')]")
    WebElement backToWorkorders;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[12]")
    List<WebElement> contractStatus;

    int rows = tableRows.size();

    public void selectPendingWorkorderWithStatus(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(pmoOfferJobName.get(i)).contains(jobName) && getTextValue(ndaStatus.get(i)).contains(status)) {
                       clickElement(actionIcon.get(i));
                        break;
                    }
            }
        }

    public void selectHmPendingWorkOrder(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(hmWorkorderJobName.get(i)).contains(jobName) && getTextValue(hmWorkorderStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void selectPmoPendingWorkOrder(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(pmoWorkorderJobName.get(i)).contains(jobName) && getTextValue(pmoWorkorderStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void verifyContractStatus(String jobName, String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(hmWorkorderJobName.get(i)).contains(jobName) && getTextValue(contractStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void selectVendorPendingWorkOrder(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(vendorWorkorderJobName.get(i)).contains(jobName) && getTextValue(vendorWorkorderStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void pmoSelectNDAComplete(String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for (int i = 0; i < rows; i++) {
            if (getTextValue(ndaStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
            }
        }
    }

    public void fillBackgroundDetails() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < backgroundList.size(); i++) {
            if (getTextValue(backgroundChecklistName.get(i)).contains("Background Attestation Form") && getTextValue(backgroundStatus.get(i)).isEmpty()) {
                clickElement(backgroundChecklist.get(i));
                selectIndexInDropdown(bckgrndAttest, 1);
                clickElement(saveBckgrndAttest);
            }
            if (getTextValue(backgroundChecklistName.get(i)).contains("Background Check") && getTextValue(backgroundStatus.get(i)).isEmpty()) {
                clickElement(backgroundChecklist.get(i));
                selectIndexInDropdown(bckgrndCheck, 1);
                clickElement(saveBckgrndCheck);
            }
            if (getTextValue(backgroundChecklistName.get(i)).contains("Drug Screening") && getTextValue(backgroundStatus.get(i)).isEmpty()) {
                clickElement(backgroundChecklist.get(i));
                selectIndexInDropdown(bckgrndDrug, 1);
                clickElement(saveBckgrndDrug);
            }
            if (getTextValue(backgroundChecklistName.get(i)).contains("Reference checks") && getTextValue(backgroundStatus.get(i)).isEmpty()) {
                clickElement(backgroundChecklist.get(i));
                selectIndexInDropdown(bckgrndReference, 1);
                clickElement(saveBckgrndReference);
            }
            if (getTextValue(backgroundChecklistName.get(i)).contains("Privacy & Security Guide & Attestation") && getTextValue(backgroundStatus.get(i)).isEmpty()) {
                clickElement(backgroundChecklist.get(i));
                selectIndexInDropdown(bckgrndPrivacy, 1);
                clickElement(saveBckgrndPrivacy);
            }
            if (getTextValue(backgroundChecklistName.get(i)).contains("Social Security Check") && getTextValue(backgroundStatus.get(i)).isEmpty()) {
                clickElement(backgroundChecklist.get(i));
                selectIndexInDropdown(bckgrndSecurity, 1);
                clickElement(saveBckgrndSecurity);
                break;
            }
        }
    }

    public void clickSaveBackground() throws InterruptedException {
        clickElement(saveFullBackground);
        TimeUnit.MILLISECONDS.sleep(1000);
    }

    public void fillBackground() throws InterruptedException {
        fillBackgroundDetails();
        TimeUnit.MILLISECONDS.sleep(1000);
        clickSaveBackground();
    }

    public void goToWorkorderWorkflow() {
        clickElement(hmWorkorderWorkflowPage);
        waitForPageLoad();
    }

    public void goToOnboardingInfo() {
        clickElement(pmoOnboardingInfoPage);
        waitForPageLoad();
    }

    public void goToBackgroundVerification() {
        clickElement(pmoBackgroundVerificationPage);
    }

    public void selectTimesheetType(String type){
        selectValueInDropdown(timesheetTypeDrpDwn, type);
        if (onboardBtn.isDisplayed()) {
            clickOnboard();
            System.out.println("Onboard Button seen and clicked.");
        }
    }

    public void selectBackgroundAllVerified(){
        if (!pmoBackgroundVerifyCheckbox.isSelected()){
            clickElement(pmoBackgroundVerifyCheckbox);
        }
        else{
            System.out.println("Checkbox already selected.");
        }
    }

    public void backGroundVerified(){
        selectBackgroundAllVerified();
        clickBackgroundCheckReviewed();
    }

    public void clickOnPmoCreateWorkorder(){ clickElement(pmoWorkorderCreateBtn); }
    public void clickOnPmoCreateAndReleaseWorkorder(){ clickElement(pmoWorkorderCreateAndReleaseBtn); }
    public void clickOnAcceptWorkorder(){ clickElement(acceptWorkorder); }
    public void clickOnboard(){ clickElement(onboardBtn); }
    public void clickBackgroundCheckReviewed(){ clickElement(pmoBackgroundVerifyBtn); }
    public void goBackToWorkorders(){clickElement(backToWorkorders);}
}