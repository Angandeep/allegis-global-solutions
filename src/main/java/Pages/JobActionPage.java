package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class JobActionPage extends BasePage {

    public JobActionPage(WebDriver driver) { super(driver);}

    @FindBy(how= How.XPATH, xpath = "//a[@class='btn btn-sm btn-success pull-right']")
    WebElement submitApproval;

    @FindBy(how= How.XPATH, xpath = "//*[@id='one']/div/div[2]/div/table/tbody/tr[2]/td[2]/span")
    WebElement jobStatus;

    @FindBy(how= How.XPATH, xpath = "//span[@class='tag label-rejected']")
    WebElement jobStatusRejected;

    @FindBy(how= How.XPATH, xpath = "//*[@id='content']/div/div/div/div[1]/div[1]/p/span")
    WebElement vendorJobStatus;

    @FindBy(how= How.XPATH, xpath = "//a[contains(text(),'Opt-In')]")
    WebElement vendorOptIn;

    @FindBy(how= How.XPATH, xpath = "//a[contains(text(),'Add Submission')]")
    WebElement vendorAddSubmission;

    @FindBy(how= How.XPATH, xpath = "//a[@class='dropdown-toggle nav-link']")
    WebElement otherDropDown;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[3]/div/div[1]/div/div/div/div/div/div/div[2]/div/ul/li[2]/a")
    WebElement workFlowApprovals;

    @FindBy(how= How.XPATH, xpath = "/html/body/div[2]/div[3]/div/div/div[1]/div/div/div/div/div/div[2]/div/div/ul/li[7]/a")
    WebElement workFlowApproval;

    @FindBy(how= How.XPATH, xpath = "//*[@id='one']/div/div/table/tbody/tr/td[2]")
    WebElement findManager;

    @FindBy(how= How.XPATH, xpath = "//button[@class='btn btn-success btn-sm']")
    WebElement jobApprove;

    @FindBy(how= How.XPATH, xpath = "//a[@class='btn btn-sm btn-danger pull-right']")
    WebElement jobReject;

    @FindBy(how= How.XPATH, xpath = "//select[@name='rejection_reason_id']")
    WebElement jobRejectDropdown;

    @FindBy(how= How.XPATH, xpath = "//textarea[@name='rejection_notes']")
    WebElement jobRejectNotes;

    @FindBy(how= How.XPATH, xpath = "//button[@type='submit']")
    WebElement jobRejectSave;

    @FindBy(how= How.XPATH, xpath = "//a[@class='btn btn-danger btn-sm']")
    WebElement jobRejection;

    @FindBy(how= How.XPATH, xpath = "//select[@name='rejection_dropdown']")
    WebElement jobRejectionDropdown;

    @FindBy(how= How.XPATH, xpath = "//textarea[@name='reason_details']")
    WebElement jobRejectionReason;

    @FindBy(how= How.XPATH, xpath = "//button[@class='btn btn-success']")
    WebElement jobRejectionSave;

    @FindBy(how= How.XPATH, xpath = "//select[@class='form-control']")
    WebElement jobStatusDropDown;

    @FindBy(how= How.XPATH, xpath = "//a[@id='jobStatusManuallRelease']")
    WebElement releaseBtn;

    @FindBy(how= How.XPATH, xpath = "//input[@type='submit']")
    WebElement releaseJob;

    public String checkStatus(){ return getTextValue(jobStatus); }

    public String checkRejectedStatus(){
        return getTextValue(jobStatusRejected);
    }

    public String checkVendorJobStatus() { return getTextValue(vendorJobStatus); }

    public void setSubmitForApproval() { clickElement(submitApproval); }

    public void goToWorkflowApprovals(){
        clickElement(workFlowApprovals);
        waitForPageLoad();
    }

    public void goToWorkflowApproval(){
        clickElement(workFlowApproval);
        waitForPageLoad();
    }

    public void setJobApprove(){ clickElement(jobApprove); }

    public void setJobReject() { clickElement(jobReject); }

    public void setJobRejection(){ clickElement(jobRejection); }

    public void jobRejectionOption(String option) { selectValueInDropdown(jobRejectionDropdown, option);}

    public void jobRejectionNotes(String note){ setElementText(jobRejectionReason, note); }

    public void saveJobRejection() { clickElement(jobRejectionSave); }

    public void jobRejectOption(String option) { selectValueInDropdown(jobRejectDropdown, option); }

    public void jobRejectNotes(String note){ setElementText(jobRejectNotes, note);}

    public void saveJobReject(){ clickElement(jobRejectSave); }

    public void setJobRejectReason(String option, String note) {
        jobRejectOption(option);
        jobRejectNotes(note);
        saveJobReject();
    }

    public void setJobRejectionReason(String option, String note) {
        jobRejectionOption(option);
        jobRejectionNotes(note);
        saveJobRejection();
    }

    public void setVendorOptIn() { clickElement(vendorOptIn);}

    public void setVendorAddSubmission() { clickElement(vendorAddSubmission);}

    public void setJobStatus(String status) throws InterruptedException {
       selectValueInDropdown(jobStatusDropDown, status);
        if (status.contains("Release to Vendor")) {
            clickElement(releaseBtn);
            clickElement(releaseJob);
        }
    }
}