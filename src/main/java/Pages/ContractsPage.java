package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ContractsPage extends BasePage {

    public ContractsPage(WebDriver driver) { super(driver);}

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr")
    List<WebElement> tableRows;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[6]/a")
    List<WebElement> contractJobNames;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]/span")
    List<WebElement> contractStatus;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> actionIcon;

    @FindBy(how= How.XPATH, xpath = "//a[@class='btn btn-sm btn-primary pull-right m-left']")
    WebElement updateContract;

    @FindBy(how= How.XPATH, xpath = "//*[@id='s2id_update_contract_reason']")
    WebElement contractReasonDropdown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen1_search']")
    WebElement contractReasonTextBox;

    @FindBy(how = How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement contractReasonResult;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_reason_of_extension']")
    WebElement contractExtensionReasonDropdown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen2_search']")
    WebElement extensionReasonTextBox;

    @FindBy(how = How.XPATH, xpath = "//input[@id='extension_date']")
    WebElement endDate;

    @FindBy(how = How.XPATH, xpath = "//*[@id='effective_date_field']")
    WebElement rateEffectiveDate;

    @FindBy(how = How.XPATH, xpath = "//textarea[@id='extension_comment']")
    WebElement extensionNotes;

    @FindBy(how = How.XPATH, xpath = "//textarea[@id='request_notes']")
    WebElement requestNotes;

    @FindBy(how = How.XPATH, xpath = "//*[@id='update_contract_button']")
    WebElement updateContractBtn;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[5]")
    List<WebElement> contractorName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[8]/span")
    List<WebElement> contractorStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[7]/span")
    List<WebElement> hmContractorStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[3]/a/img")
    List<WebElement> contractorNotApprovedIcon;

    @FindBy(how = How.XPATH, xpath = "//div[@class='m-b-10']")
    WebElement vpContractStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='ext_table']/tbody/tr")
    List<WebElement> extensionTable;

    @FindBy(how = How.XPATH, xpath = "//*[@id='ext_table']/tbody/tr/td[5]/a")
    List<WebElement> extensionContractorName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='ext_table']/tbody/tr/td[1]/span")
    List<WebElement> extensionContractorStatus;

    int rows = tableRows.size();

    public void selectPmContractJob(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1000);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(contractJobNames.get(i)).contains(jobName) && getTextValue(contractStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void hmSelectContractorWithStatus(String name, String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++){
            if (getTextValue(contractorName.get(i)).contains(name) && getTextValue(contractorStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void selectHmContractorWithStatus(String name, String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++){
            if (getTextValue(contractorName.get(i)).contains(name) && getTextValue(hmContractorStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void vendorSelectContractorWithStatus(String name, String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < extensionTable.size(); i++){
            if (getTextValue(extensionContractorName.get(i)).contains(name) && getTextValue(extensionContractorStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void clickOnUpdateContract(){clickElement(updateContract);}

    public void updateContractReason(String reason) throws InterruptedException {
        clickElement(contractReasonDropdown);
        setElementText(contractReasonTextBox,reason);
        String resultText = getTextValue(contractReasonResult);
        if (resultText.contains(reason)){
            clickElement(contractReasonResult);
            TimeUnit.MILLISECONDS.sleep(200);
            System.out.println("Reason selected as " + reason);
        } else {
            System.out.println("Entered reason not found....");
        }
    }

    public void selectExtensionReason(String Extensionreason) throws InterruptedException {
        clickElement(contractExtensionReasonDropdown);
        setElementText(extensionReasonTextBox,Extensionreason);
        String resultText = getTextValue(contractReasonResult);
        if (resultText.contains(Extensionreason)){
            clickElement(contractReasonResult);
            TimeUnit.MILLISECONDS.sleep(200);
            System.out.println("Reason selected as " + Extensionreason);
        } else {
            System.out.println("Entered reason not found....");
        }
    }

    public void contractExtendedEndDate(){
        String dateVal ="12/30/2021";
        selectDate(driver, endDate, dateVal);
    }

    public void effectiveDateRateChange(String dateVal){
        dateVal =futureDayTomorrow();
        selectDate(driver, rateEffectiveDate, dateVal);
        clickUpdateContractBtn();
    }

    public void addExtensionNotes(){
        extensionNotes.click();
        extensionNotes.sendKeys("abc");
    }
    public void clickUpdateContractBtn(){ clickElement(updateContractBtn);}
    public String checkVpContractStatus(){ return getTextValue(vpContractStatus);}
    public void addRequestNotes(String notes){
        requestNotes.click();
        requestNotes.sendKeys(notes);
        clickUpdateContractBtn();
    }

    public void extendContract(String Extensionreason) throws InterruptedException {
        selectExtensionReason(Extensionreason);
        contractExtendedEndDate();
        addExtensionNotes();
        clickUpdateContractBtn();
    }
}