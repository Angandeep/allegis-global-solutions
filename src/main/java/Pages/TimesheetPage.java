package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TimesheetPage extends BasePage {

    public TimesheetPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how= How.XPATH, xpath = "//a[@class='select2-choice']")
    WebElement contractorDropdown;

    @FindBy(how= How.XPATH, xpath = "//*[@id='s2id_autogen2_search']")
    WebElement contractorSearchTextbox;

    @FindBy(how = How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement contractorSearchResults;

    @FindBy(how= How.XPATH, xpath = "//*[@id='content']/form/div/div[1]/div/div/div/div[2]/div/input")
    WebElement continueContractor;

    @FindBy(how= How.XPATH, xpath = "//*[@id='select2-chosen-1']")
    WebElement timesheetdateDropdown;

    @FindBy(how= How.XPATH, xpath = "//*[@id='s2id_autogen1_search']")
    WebElement timesheetDateSearchBox;

    @FindBy(how= How.XPATH, xpath = "//*[@id='content']/form/div/div[1]/div/div/div/div[3]/p/input")
    WebElement timesheetContinue;

    @FindBy(how = How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement timesheetSearchResults;

    @FindBy(how = How.XPATH, xpath = "//*[@id='start_time_mon']")
    WebElement timeInMon;

    @FindBy(how = How.XPATH, xpath = "//*[@id='start_time_tue']")
    WebElement timeInTue;

    @FindBy(how = How.XPATH, xpath = "//*[@id='start_time_wed']")
    WebElement timeInWed;

    @FindBy(how = How.XPATH, xpath = "//*[@id='start_time_thu']")
    WebElement timeInThu;

    @FindBy(how = How.XPATH, xpath = "//*[@id='start_time_fri']")
    WebElement timeInFri;

    @FindBy(how = How.XPATH, xpath = "//*[@id='end_time_mon']")
    WebElement timeOutMon;

    @FindBy(how = How.XPATH, xpath = "//*[@id='end_time_tue']")
    WebElement timeOutTue;

    @FindBy(how = How.XPATH, xpath = "//*[@id='end_time_wed']")
    WebElement timeOutWed;

    @FindBy(how = How.XPATH, xpath = "//*[@id='end_time_thu']")
    WebElement timeOutThu;

    @FindBy(how = How.XPATH, xpath = "//*[@id='end_time_fri']")
    WebElement timeOutFri;

    @FindBy(how = How.XPATH, xpath = "//*[@id='total_hours_sum']")
    WebElement timesheetFillTotalHrs;

    @FindBy(how = How.XPATH, xpath = "//*[@id='saveSheet_2_vert']")
    WebElement saveTimesheet;

    @FindBy(how = How.XPATH, xpath = "//button[@name='submit_timeSheet']")
    WebElement submitTimesheet;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr")
    List<WebElement> tableRows;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[4]")
    List<WebElement> timesheetContractorName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]/span")
    List<WebElement> hmTimesheetStatus;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> actionIcon;

    @FindBy(how = How.XPATH, xpath = "//table[@class='table without-border']/tbody/tr")
    List<WebElement> timesheetFilledTable;

    @FindBy(how = How.XPATH, xpath = "//table[@class='table without-border']/tbody/tr[3]/td[1]")
    List<WebElement> timesheetTotalHrs;

    @FindBy(how = How.XPATH, xpath = "//table[@class='table without-border']/tbody/tr[3]/td[9]")
    WebElement timesheetBllableHrs;

    @FindBy(how = How.XPATH, xpath = "//table[@class='table without-border']/tbody/tr[3]/td[10]")
    WebElement timesheetTotalAmount;

    @FindBy(how = How.XPATH, xpath = "//table[@class='table sub-table  tab-table gray-table  m-b-0 smtable-uiv2 uiv2-linetable']/tbody/tr[3]/td[2]")
    WebElement timesheetApproverName;

    @FindBy(how = How.XPATH, xpath = "//button[@name='approve']")
    WebElement approveTimesheet;

    @FindBy(how = How.XPATH, xpath = "//a[@id='timesheet_reject_button']")
    WebElement rejectTImesheet;

    @FindBy(how = How.XPATH, xpath = "//div[@class='col-md-3'][1]")
    WebElement contractorAddTimesheet;

    @FindBy(how = How.XPATH, xpath = "//select[@name='reason_for_rejection']")
    WebElement timesheetRejectionDropdown;

    @FindBy(how= How.XPATH, xpath = "//textarea[@name='rejection_notes']")
    WebElement timesheetRejectNotes;

    @FindBy(how= How.XPATH, xpath = "//*[@name='reject']")
    WebElement saveRejectTimesheet;

    int rows = tableRows.size();

    public String totalFilledBillForTImesheet(String amount) throws InterruptedException {
        String totalBillAmount = getTextValue(timesheetTotalAmount);
        if (totalBillAmount.contains(amount)){
            System.out.println("Bill found.");
        }
        else{
            System.out.println("Bill not found!");
        }
        return amount;
    }

    public String totalFilledHoursForTImesheet(String hours) throws InterruptedException {
        String totalHours = getTextValue(timesheetBllableHrs);
        if (totalHours.contains(hours)){
            System.out.println("Hours found.");
        }
        else{
            System.out.println("Hours not found!");
        }
        return hours;
    }

    public String timesheetApproverName(String approver) throws InterruptedException {
        String approverName = getTextValue(timesheetApproverName);
        if (approverName.contains(approver)){
            System.out.println("Approver found.");
        }
        else{
            System.out.println("approver not found!");
        }
        return approver;
    }

    public void hmSelectApproveTimesheet(String name, String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++){
            if (getTextValue(timesheetContractorName.get(i)).contains(name) && getTextValue(hmTimesheetStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void selectContractor(String contractorName) throws InterruptedException {
        waitForPageLoad();
        clickElement(contractorDropdown);
        setElementText(contractorSearchTextbox,contractorName);
        String resultText = getTextValue(contractorSearchResults);
        if (resultText.contains(contractorName)){
            clickElement(contractorSearchResults);
            TimeUnit.MILLISECONDS.sleep(2000);
            clickElement(continueContractor);
            System.out.println("Contractor selected as " + contractorName);
        } else {
            System.out.println("Entered Contractor not found....");
        }
    }

    public void selectTimesheetDate(String dateTime) throws InterruptedException {
        clickElement(timesheetdateDropdown);
        setElementText(timesheetDateSearchBox,dateTime);
        String resultText = getTextValue(timesheetSearchResults);
        if (resultText.contains(dateTime)){
            clickElement(timesheetSearchResults);
            TimeUnit.MILLISECONDS.sleep(2000);
            clickElement(timesheetContinue);
            System.out.println("Date selected as " + dateTime);
        } else {
            System.out.println("Entered Date not found....");
        }
    }

    public void enterInTIme(String timeEnter) throws InterruptedException {
        setElementText(timeInMon, timeEnter);
        setElementText(timeInTue, timeEnter);
        setElementText(timeInWed, timeEnter);
        setElementText(timeInThu, timeEnter);
        setElementText(timeInFri, timeEnter);
        TimeUnit.MILLISECONDS.sleep(2000);
    }

    public void enterOutTIme(String timeOut) throws InterruptedException {
        setElementText(timeOutMon, timeOut);
        setElementText(timeOutTue, timeOut);
        setElementText(timeOutWed, timeOut);
        setElementText(timeOutThu, timeOut);
        setElementText(timeOutFri, timeOut);
        TimeUnit.MILLISECONDS.sleep(1000);
    }

    public void setSaveTimesheet(){ clickElement(saveTimesheet);}

    public void setInAndOutTIme(String entryTime, String outTime) throws InterruptedException {
        enterInTIme(entryTime);
        enterOutTIme(outTime);
        setSaveTimesheet();
    }

    public void setSubmitTimesheet(){ clickElement(submitTimesheet);}
    public void setHmApproveTimesheet(){ clickElement(approveTimesheet);}
    public void setHmRejectTimesheet(){ clickElement(rejectTImesheet);}

    public String totalFilledHours(String hours){
        String message = getTextValue(timesheetFillTotalHrs);
        if (message.contains(hours)){
            System.out.println("Total hours found.");
        }
        else{
            System.out.println("Hours not found!");
        }
        return hours;
    }

    public void timesheetRejectionOption(String option) { selectValueInDropdown(timesheetRejectionDropdown, option);}
    public void timesheetRejectionNotes(String note){ setElementText(timesheetRejectNotes, note); }
    public void saveTimesheetRejection() { clickElement(saveRejectTimesheet); }

    public void setTimesheetRejectionReason(String option, String note) {
        timesheetRejectionOption(option);
        timesheetRejectionNotes(note);
        saveTimesheetRejection();
    }
}