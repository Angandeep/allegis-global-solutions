package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SubmissionActionPage extends BasePage {

    public SubmissionActionPage(WebDriver driver) {super(driver);}

    @FindBy(how = How.XPATH, xpath = "//*[@id='content']/div/div/div/div[2]/div/ul/li[2]/a")
    WebElement submissionPageLink;

    @FindBy(how = How.XPATH, xpath = "//span[@class='tag label-hold']")
    WebElement submissionStatus;

    @FindBy(how = How.XPATH, xpath = "//span[@class='tag label-new-request']")
    WebElement shortlistStatus;

    @FindBy(how = How.XPATH, xpath = "//p[@class='m-b-10']")
    WebElement clientReviewStatus;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    WebElement actionIcon;

    @FindBy(how = How.XPATH, xpath = "//span[@class='tag label-filled']")
    WebElement actionStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]")
    List<WebElement> reviewJobStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[6]/a")
    List<WebElement> reviewJobNames;

    @FindBy(how = How.XPATH, xpath = "//button[@name='shortlist_submission']")
    WebElement shortlist;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[4]")
    List<WebElement> shortlistedName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]/a")
    List<WebElement> shortlistName;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm btn-success pull-right m-left']")
    WebElement scheduleInterview;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[4]")
    List<WebElement> shortlistedJobName;

    @FindBy(how= How.XPATH, xpath = "//textarea[@name='notes']")
    WebElement resumeRejectNotes;

    @FindBy(how= How.XPATH, xpath = "//*[@id='reject-can']/div/div/form/div[2]/button[1]")
    WebElement resumeRejectSave;

    @FindBy(how= How.XPATH, xpath = "//button[@name='reject_offer']")
    WebElement offerRejectSave;

    @FindBy(how = How.XPATH, xpath = "//select[@name='reason_for_rejection']")
    WebElement resumeRejectionDropdown;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm btn-danger pull-right']")
    WebElement rejectCandidate;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm btn-danger pull-right m-left'][2]")
    WebElement pmoRejectCandidate;

    @FindBy(how= How.XPATH, xpath = "//*[@id='modal-id1']/div/form/div/div[3]/button[1]")
    WebElement interviewRejectSave;

    @FindBy(how= How.XPATH, xpath = "(//textarea[@name='notes'])[2]")
    WebElement InterviewRejectNotes;

    @FindBy(how = How.XPATH, xpath = "//select[@name='rejection_type']")
    WebElement interviewRejectionDropdown;

    public void goToSubmissions() {
        clickElement(submissionPageLink);
        waitForPageLoad();
    }

    public String checkSubmissionStatus() { return getTextValue(submissionStatus); }

    public void clickOnActionIcon() {
        clickElement(actionIcon);
        waitForPageLoad();
    }

    public String checkStatus() { return getTextValue(actionStatus); }

    public String checkShortlisted() { return getTextValue(shortlistStatus); }

    public String checkClientReviewStatus() { return getTextValue(clientReviewStatus);}

    public void openReviewJobIdDetails(String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for (int i = 0; 1 < reviewJobNames.size(); i++) {
            if (getTextValue(reviewJobNames.get(i)).contains(jobName)) {
                clickElement(reviewJobNames.get(i));
                break;
            }
        }
    }

    public void openShortlistedUserDetails(String userName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for (int i = 0; 1 < shortlistedName.size(); i++) {
            if (getTextValue(shortlistedName.get(i)).contains(userName)) {
                clickElement(shortlistedName.get(i));
                break;
            }
        }
    }

    public void openShortlistedJobDetails(String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for (int j = 0; j < shortlistedJobName.size(); j++) {
            if (getTextValue(shortlistedJobName.get(j)).contains(jobName)) {
                clickElement(actionIcon);
                    break;
                }
            }
        }

    public void scheduleInterview() { clickElement(scheduleInterview);}
    public void shortListResume(){ clickElement(shortlist);}
    public void rejectCandidate(){ clickElement(rejectCandidate);}
    public void pmoRejectCandidate(){ clickElement(pmoRejectCandidate);}

    public void rejectSubmissionReason(String reason){
        selectValueInDropdown(resumeRejectionDropdown, reason);
    }

    public void setRejectionNotes(String notes){
        setElementText(resumeRejectNotes, notes);
    }

    public void rejectResumeSave(){clickElement(resumeRejectSave);}
    public void rejectOfferSave(){clickElement(offerRejectSave);}

    public void setResumeReject(String reason, String notes){
        rejectSubmissionReason(reason);
        setRejectionNotes(notes);
        rejectResumeSave();
    }

    public void setOfferReject(String reason, String notes){
        rejectSubmissionReason(reason);
        setRejectionNotes(notes);
        rejectOfferSave();
    }

    public void rejectInterviewReason(String reason){
        selectValueInDropdown(interviewRejectionDropdown, reason);
    }

    public void setInterviewRejectionNotes(String notes){
        setElementText(InterviewRejectNotes, notes);
    }

    public void rejectInterviewSave(){clickElement(interviewRejectSave);}

    public void setInterviewReject(String reason, String notes){
        rejectInterviewReason(reason);
        setInterviewRejectionNotes(notes);
        rejectInterviewSave();
    }

}