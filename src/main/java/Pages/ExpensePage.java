package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExpensePage extends BasePage {

    public ExpensePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.XPATH, xpath = "//*[@id='select2-chosen-1']")
    WebElement locationDropdown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='s2id_autogen1_search']")
    WebElement locationSearchBox;

    @FindBy(how = How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement locationSearchResult;

    @FindBy(how = How.XPATH, xpath = "/html/body/div[1]/div[3]/div/div/div[1]/div/div/div/form/div/div/div[1]/div/div/div[2]/div/div/a/span[1]")
    WebElement contractorDropdown;

    @FindBy(how = How.XPATH, xpath = "/html/body/div[12]/div/input")
    WebElement contractorSearchTextbox;

    @FindBy(how = How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement contractorSearchResults;

    @FindBy(how = How.XPATH, xpath = "//*[@id='content']/form/div/div/div[1]/div/div/div[3]/div/input")
    WebElement continueContractor;

    @FindBy(how = How.XPATH, xpath = "//*[@id='expense_date']")
    WebElement expenseDateDropdown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='expense_type']")
    WebElement expenseTypeDropdown;

    @FindBy(how = How.XPATH, xpath = "//*[@id='amount']")
    WebElement expenseAmount;

    @FindBy(how = How.XPATH, xpath = "//button[@name='expenseStep2']")
    WebElement addExpense;

    @FindBy(how = How.XPATH, xpath = "//a[contains(text(),'Submit for Approval')]")
    WebElement submitForApproval;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr")
    List<WebElement> tableRows;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[3]")
    List<WebElement> expenseContractorName;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[1]/span")
    List<WebElement> hmExpenseStatus;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> actionIcon;

    @FindBy(how = How.XPATH, xpath = "//button[@name='approve']")
    WebElement approveExpense;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-danger']")
    WebElement rejectExpense;

    int rows = tableRows.size();

    public void hmSelectApproveExpense(String name, String status) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++){
            if (getTextValue(expenseContractorName.get(i)).contains(name) && getTextValue(hmExpenseStatus.get(i)).contains(status)){
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }


    public void selectLocation(String location) throws InterruptedException {
        waitForPageLoad();
        clickElement(locationDropdown);
        setElementText(locationSearchBox, location);
        String resultText = getTextValue(locationSearchResult);
        if (resultText.contains(location)) {
            clickElement(locationSearchResult);
            TimeUnit.MILLISECONDS.sleep(2000);
            System.out.println("Location selected as " + location);
        } else {
            System.out.println("Entered Location not found....");
        }
    }

    public void selectContractor(String contractorName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        clickElement(contractorDropdown);
        setElementText(contractorSearchTextbox, contractorName);
        String resultText = getTextValue(contractorSearchResults);
        if (resultText.contains(contractorName)) {
            clickElement(contractorSearchResults);
            System.out.println("Contractor selected as " + contractorName);
        } else {
            System.out.println("Entered Contractor not found....");
        }
    }

    public void continueWithLocationAndContractor() {
        clickElement(continueContractor);
    }

    public void setLocationAndContractor(String location, String contractorName) throws InterruptedException {
        selectLocation(location);
        selectContractor(contractorName);
        continueWithLocationAndContractor();
    }

    public void selectExpenseDate(String day) {
        clickElement(expenseDateDropdown);
        selectValueInDropdown(expenseDateDropdown, day);
    }

    public void selectExpenseType(String type) {
        clickElement(expenseTypeDropdown);
        selectValueInDropdown(expenseTypeDropdown, type);
    }

    public void setExpenseAmount(String amount) {
        setElementText(expenseAmount, amount);
    }

    public void addExpense() { clickElement(addExpense); }

    public void setAddExpense(String day, String type, String amount){
        selectExpenseDate(day);
        selectExpenseType(type);
        setExpenseAmount(amount);
        addExpense();
    }

    public void setSubmitForApproval(){clickElement(submitForApproval);}
    public void setApproveExpense(){clickElement(approveExpense);}
    public void setRejectExpense(){clickElement(rejectExpense);}
}