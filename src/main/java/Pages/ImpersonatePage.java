package Pages;

import Base.BasePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.concurrent.TimeUnit;

public class ImpersonatePage extends BasePage {
    LoginPage loginPage = new LoginPage(BasePage.driver);

    public ImpersonatePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how= How.XPATH, xpath = "//*[@class='select2-choice']")
    WebElement userDropDown;

    @FindBy(how= How.XPATH, xpath = "//h4[@class='m-b-10']")
    WebElement impersonateText;

    @FindBy(how= How.XPATH, xpath = "//*[@id='s2id_autogen1_search']")
    WebElement userDropDownTextBox;

    @FindBy(how= How.XPATH, xpath = "//div[@class='select2-result-label']")
    WebElement userDropDownResult;

    @FindBy(how= How.XPATH, xpath = "//button[@class='btn btn-success']")
    WebElement impersonateUserBtn;

    @FindBy(how= How.XPATH, xpath = "//*[@class='impersonate-button']")
    WebElement impersonatedUserName;

    public void impersonateUser(String user) {
        loginPage.ClickOnAvatar();
        loginPage.ClickOnImpersonateOption();
        waitForPageLoad();
        Assert.assertEquals("Impersonate", driver.getTitle());
        clickElement(userDropDown);
        System.out.println("Entering username to impersonate.....");
        setElementText(userDropDownTextBox, user);
        String resultText = getTextValue(userDropDownResult);
        if (resultText.contains(user)){
            clickElement(userDropDownResult);
            clickElement(impersonateUserBtn);
        } else {
            System.out.println("Entered user not found....");
        }
    }

    public void impersonateAsProgram() throws InterruptedException {
        clickElement(impersonatedUserName);
        TimeUnit.MILLISECONDS.sleep(1000);
    }
}