package Pages;

import Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {
    public WebDriver driver;
    LoginPage loginPage;
    SignNDAPage signNDAPage;

    public LoginPage(WebDriver driver){
        super(driver);
        this.PAGE_URL = "https://qa-ags.simplifyvms.com/program";
        this.PAGE_TITLE = "VMS-Program-Login";
    }

    @FindBy(how= How.XPATH, xpath = "//input[@id='username']")
    WebElement username;

    @FindBy(how= How.XPATH, xpath = "//input[@id='password']")
    WebElement password;

    @FindBy(how= How.XPATH, xpath = "//button[@type='submit']")
    WebElement loginbtn;

    @FindBy(how= How.XPATH, xpath = "//*[@class='text-center']/img[@src]")
    WebElement loginpagelogo;

    @FindBy(how= How.XPATH, xpath = "//img[@class='avatar img-circle']")
    WebElement avatar;

    @FindBy(how= How.XPATH, xpath = "//a[@class='dropdown-item'][contains(text(),'Impersonate')]")
    WebElement impersonate;

    @FindBy(how= How.XPATH, xpath = "//a[@class='dropdown-item'][contains(text(),'Logout')]")
    WebElement logoutBtn;

    @FindBy(how= How.XPATH, xpath = "//input[@name='old_password']")
    WebElement currentPwdField;

    @FindBy(how= How.XPATH, xpath = "//input[@name='new_password']")
    WebElement newPasswordField;

    @FindBy(how= How.XPATH, xpath = "//input[@name='repeat_password']")
    WebElement confirmPassword;

    @FindBy(how= How.XPATH, xpath = "//button[@name='updatepassword']")
    WebElement updatePwdBtn;

    public void openLoginPage(){
        driver.get(PAGE_URL);
    }

    public void EnterUser(String user){
        setElementText(username, user);
    }

    public void EnterPwd(String pwd){
       setElementText(password, pwd);
    }

    public void ClickLoginBtn(){
        clickElement(loginbtn);
    }

    public void ClickOnAvatar(){
        clickElement(avatar);
    }

    public void ClickOnImpersonateOption(){
        clickElement(impersonate);
    }

    public void login(String user, String pwd){
        EnterUser(user);
        EnterPwd(pwd);
        ClickLoginBtn();
    }

    public void loginAsPMO(){
        loadPMOLoginPage();
        EnterUser("simplifymspalert@gmail.com");
        EnterPwd("yahoo2017");
        ClickLoginBtn();
        waitForPageLoad();
    }

    public void logout() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(2000);
        ClickOnAvatar();
        clickElement(logoutBtn);
    }

    public void contractorChangePassword(String newPassword){
        setElementText(currentPwdField, signNDAPage.getSentPassword());
        setElementText(newPasswordField, newPassword);
        setElementText(confirmPassword, newPassword);
        clickElement(updatePwdBtn);
    }
}