package Pages;

import Base.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class OfferApprovalPage extends BasePage {

    public OfferApprovalPage(WebDriver driver) { super(driver); }
    LoginPage loginPage;
    SignNDAPage signNDAPage;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[1]")
    List<WebElement> offerJobStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[3]")
    List<WebElement> offerJobName;

    @FindBy(how = How.XPATH, xpath = "//i[@class='icon svg-icon']")
    List<WebElement> actionIcon;

    @FindBy(how = How.XPATH, xpath = "//button[@name='approve']")
    WebElement approveOffer;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm btn-danger']")
    WebElement rejectOffer;

    @FindBy(how = How.XPATH, xpath = "//a[@class=' btn btn-sm btn-danger']")
    WebElement rejectExtension;

    @FindBy(how = How.XPATH, xpath = "//*[@id='reject']")
    WebElement vendorRejectOffer;

    @FindBy(how = How.XPATH, xpath = "(//td[@class='actions text-center'])[1]")
    WebElement actionStatus;

    @FindBy(how = How.XPATH, xpath = "//span[@class='tag label-default']")
    WebElement offerStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[2]")
    List<WebElement> pmoOfferReviewStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[4]")
    List<WebElement> pmoOfferJobName;

    @FindBy(how = How.XPATH, xpath = "//a[@class='btn btn-sm btn-success m-right']")
    WebElement pmoOfferReleaseBtn;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[1]")
    List<WebElement> vendorOfferStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr/td[3]")
    List<WebElement> vendorOfferJobName;

    @FindBy(how = How.XPATH, xpath = "//input[@name='can_digitalsign_email']")
    WebElement contractorEmailField;

    @FindBy(how = How.XPATH, xpath = "//*[@id='statusAccepted']")
    WebElement vendorAcceptOffer;

    @FindBy(how = How.XPATH, xpath = "//p[@class='m-b-10']")
    WebElement vendorOfferApprovedStatus;

    @FindBy(how = How.XPATH, xpath = "(//p[@class='m-b-10'])[1]")
    WebElement pmoOfferApprovedStatus;

    @FindBy(how = How.XPATH, xpath = "//*[@id='search_table']/tbody/tr")
    List<WebElement> tableRows;

    @FindBy(how= How.XPATH, xpath = "//select[@name='reason']")
    WebElement offerRejectionDropdown;

    @FindBy(how= How.XPATH, xpath = "//button[@name='rejectofferworkflow']")
    WebElement offerRejectionSave;

    @FindBy(how= How.XPATH, xpath = "(//textarea[@name='notes'])[1]")
    WebElement offerRejectionNotes;

    @FindBy(how= How.XPATH, xpath = "//*[@id='note']")
    WebElement extensionRejectionNotes;

    @FindBy(how= How.XPATH, xpath = "//button[@name='rejectbill']")
    WebElement extensionRejectionSave;

    int rows = tableRows.size();

    public void selectOfferJobWithStatus(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(offerJobName.get(i)).contains(jobName) && getTextValue(offerJobStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void selectPmoOfferJobWithStatus(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(pmoOfferJobName.get(i)).contains(jobName) && getTextValue(pmoOfferReviewStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void selectVendorOfferJobWithStatus(String status, String jobName) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        for(int i = 0; i < rows; i++) {
            if (getTextValue(vendorOfferJobName.get(i)).contains(jobName) && getTextValue(vendorOfferStatus.get(i)).contains(status)) {
                clickElement(actionIcon.get(i));
                break;
            }
        }
    }

    public void approveJobOffer(){ clickElement(approveOffer); }
    public void rejectJobOffer(){ clickElement(rejectOffer); }
    public void rejectExtension(){ clickElement(rejectExtension); }
    public void vendorRejectOffer(){ clickElement(vendorRejectOffer); }
    public String checkOfferStatus() { return getTextValue(offerStatus); }
    public String checkActionStatus() { return getTextValue(actionStatus); }
    public void clickOnPmoReleaseOffer(){ clickElement(pmoOfferReleaseBtn); }
    public String randomestring(){ return(RandomStringUtils.randomAlphabetic(4)); }
    public String randomEmailAddress(){
        String email = randomestring()+"@gmail.com";
        return email;
    }
    public void fillEmail(){ setElementText(contractorEmailField, randomEmailAddress()); }

    public void loginAsContractor() throws InterruptedException {
        goToNewWindow();
        loginPage.EnterUser(randomEmailAddress());
        loginPage.EnterPwd(signNDAPage.getSentPassword());
        loginPage.ClickLoginBtn();
    }

    public void fillContractorEmail(){ setElementText(contractorEmailField, "abc.xyz.simplifyvms+" +  System.currentTimeMillis() + "@gmail.com"); }
    public void clickAcceptOfferAsVendor(){ clickElement(vendorAcceptOffer);}
    public String checkVendorApprovedStatus(){ return getTextValue(vendorOfferApprovedStatus);}
    public String checkPmoApprovedStatus(){ return getTextValue(pmoOfferApprovedStatus);}

    public void vendorAcceptOffer(){
        fillContractorEmail();
        clickAcceptOfferAsVendor();
    }

    public void vendorRejectedOffer(){
        fillContractorEmail();
        vendorRejectOffer();
    }

    public void offerRejectionOption(String option) { selectValueInDropdown(offerRejectionDropdown, option);}
    public void offerRejectionNotes(String note){ setElementText(offerRejectionNotes, note); }
    public void saveOfferRejection() { clickElement(offerRejectionSave); }
    public void extensionRejectionNotes(String note){ setElementText(extensionRejectionNotes, note); }
    public void saveExtensionRejection() { clickElement(extensionRejectionSave); }

    public void setOfferRejectionReason(String option, String note) {
        offerRejectionOption(option);
        offerRejectionNotes(note);
        saveOfferRejection();
    }

    public void setExtensionRejectionReason(String option, String note) {
        offerRejectionOption(option);
        extensionRejectionNotes(note);
        saveExtensionRejection();
    }
}