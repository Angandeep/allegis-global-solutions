@releaseOffer

Feature: Release Offer

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Release Offer by PMO
    Given I click on Approved Offers Pending PMO Release in dashboard
    When I select the created job with status PMO - Review
    Then I'm clicking on Release Offer
    And I check success alert is shown with message Offer released successfully.