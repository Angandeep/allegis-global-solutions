@contractEntension @regression

  Feature: Contract Extension

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: PMO creating Contract Extension
    Given I click on Contracts in dashboard
    When I select the created job with status Approved
    Then I'm clicking on Update Contract
    And I select update reason as Extension with extension reason as Assignment Scope Change
    And I check success alert is shown with message Contract will be updated once the approval process is successful.

  Scenario: Hiring Manager Approve Contract Extension
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract Extensions Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.

  Scenario: VP Approve Contract Extension
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Contract Extensions Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.
    And I check that contract status is Supplier Pending

  Scenario: Vendor Approve Contract Extension
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Contract Extension in dashboard
    Then I select the approval job with status Supplier Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.