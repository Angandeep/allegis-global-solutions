@smoke @contractAmendments

Feature: Contract Amendments

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: PMO Creating Contract Amendment
    Given I click on Contracts in dashboard
    When I select the created job with status Approved
    Then I'm clicking on Update Contract
    And I select update reason as Rate Change with effective date as tomorrow
    And I check success alert is shown with message Contract will be updated once the approval process is successful.

  Scenario: Hiring Manager Approve Contract Amendment
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract Amendments Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.

  Scenario: VP approving Contract Amendment
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Contract Amendments Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.
    And I check that contract status is Approved


# Bug found in AGS QA: The approval is not needed from Vendor side(Build not updated)

#  Scenario: Vendor approving Contract Amendment
#    Given I impersonate as Vendor with name Kelly Staffing Agency
#    When I click on Contract Amendments Pending Approval in dashboard
#     Then I select the approval job with status Supplier Pending
#    And I Approve the offer
#    And I check success alert is shown with message Workflow has been approved successfully.

  Scenario: PMO Creating Contract Amendment for Non Financial Change
    Given I click on Contracts in dashboard
    When I select the created job with status Approved
    Then I'm clicking on Update Contract
    And I select update reason as Non Financial Change with notes as Not related to finance
    And I check success alert is shown with message Contract updated successfully.

  Scenario: Hiring Manger creating Contract Amendment for Non Financial Change
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract in dashboard
    Then I select the approval job with status Approved
    And I'm clicking on Update Contract
    And I select update reason as Non Financial Change with notes as Not related to finance
    And I check success alert is shown with message Contract updated successfully.