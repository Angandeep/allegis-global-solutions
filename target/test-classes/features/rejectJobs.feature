@regression @rejectJobs

Feature: Reject Jobs

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: PMO Reject the Job
    Given I click on Jobs Pending Review in dashboard
    When I select the created job with status Pending - PMO
    And I Reject the job
    And I give reason as Job Posting Cancelled and comments as DEF
    And I check success alert is shown with message Job is rejected successfully.
    Then I check the status is Rejected

  Scenario: Rejection by “Hiring Manager”
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Jobs Pending Approval in dashboard
    Then I select the approval job with status Pending Approval - Open
    And I go to Workflow Approval page
    And I reject the job
    And I'm giving reason as Job Posting Cancelled and comments as DEF
    And I check the status is Rejected

  Scenario: VP Reject the Job
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Jobs Pending Approval in dashboard
    Then I select the approval job with status Pending Approval - Open
    And I go to Workflow Approvals page
    And I reject the job
    And I'm giving reason as Job Posting Cancelled and comments as DEF
    And I check the status is Rejected