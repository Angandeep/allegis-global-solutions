@acceptInterview

Feature: Accept Interview

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Accept Interview by Vendor
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Interviews in dashboard
    Then I select the approval job with status Waiting for Approval
    And I Accept interview
    And I check success alert is shown with message You have successfully accepted the interview.
    And I check that interview status is Approved