@submitJobForApproval

Feature: Submit Job For Approval

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: PMO Submit for Approval
    When I click on Jobs Pending Review in dashboard
    And I select the created job with status Pending - PMO
    And I Submit For Approval the job
    And I check success alert is shown with message Job Updated Successfully.
    Then I check the status is Pending Approval - Open