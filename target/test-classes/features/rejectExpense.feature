@rejectExpense @regression

Feature: Reject Expense

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Timesheet Approving Manager Reject Expense
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Expense Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I'm clicking on Reject Expense
    And I give Reject reason as Missing or Incorrect Information and notes as ABC
    And I check danger alert is shown with message Missing or Incorrect Information