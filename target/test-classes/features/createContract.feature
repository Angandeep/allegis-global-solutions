@createContract

Feature: Create Contract

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: PMO creating Contract
    Given I click on Validate Onboarding Completion in dashboard
    When I select the created job with status Approved
    Then I go to Onboarding Info page
    And I select Timesheet Type as Timesheet with Clock System
    Then I go to Background Verification page
    And I'm clicking on Background Check Reviewed
    And I check success alert is shown with message Background verification marked as reviewed.
    Then I go to Back to List of Workorders page
    When I select job with name Audit Manager and Contract status as OnBoarded