@regression @rejectContractAmendments

Feature: Reject Contract Amendments

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Hiring Manager Reject Contract Amendment
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract Amendments Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I Reject the offer
    And I select reason for reject as Additional budget not available and comments as abc
    And I check danger alert is shown with message You have rejected workflow successfully.
    And I check that contract status is Rejected

  Scenario: VP Reject Contract Amendment
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Contract Amendments Pending Approval in dashboard
    Then I select the approval job with status Pending
    And I Reject the offer
    And I select reason for reject as Additional budget not available and comments as abc
    And I check danger alert is shown with message You have rejected workflow successfully.
    And I check that contract status is Rejected

# Bug found in AGS QA: The approval is not needed from Vendor side(Build not updated)
#
#  Scenario: Vendor rejecting Contract Amendment
#    Given I impersonate as Vendor with name Kelly Staffing Agency
#    When I click on Contract Amendments Pending Approval in dashboard
#    Then I select contractor with name Sonia Anderson and status Pending
#    And I Reject the offer
#    And I select reason for reject as Additional budget not available and comments as abc
#    And I check danger alert is shown with message You have rejected workflow successfully.
#    And I check that contract status is Rejected