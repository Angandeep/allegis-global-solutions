@submissionReview

Feature: Submission Review

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: PMO Submission Review and shortlist
    When I click on Resumes To Review in dashboard
    Then I select the created job with status Submitted
    And I check the status is Sourcing
    And I go to Submissions page
    And I check that submission status is Submitted
    And I'm clicking on Action
    And I check success alert is shown with message The following status has been updated to MSP Reviewed.
    And I check that submission status is MSP Reviewed
    And I'm clicking on Shortlist
    And I check success alert is shown with message You have successfully Shortlisted a candidate
    And I check that submission status is Shortlisted

  @smoke
  Scenario: Submission Review by Hiring Manager and Schedule Interview
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Resumes To Review in dashboard
    Then I select the approval job with status Shortlisted
    And I check that submission status is Client Review
    And I'm clicking on Schedule Interview
    And I schedule Interview with duration 30 Minutes and type Personal Interview
    And I select slot as 10:00 AM - 10:30 AM
    And I check success alert is shown with message Your Interview Posting was successfully submitted.
    And I check that submission status is Waiting for Approval

  @regression
  Scenario: Submission Rejection by PMO
    When I click on Resumes To Review in dashboard
    Then I select the approval job with status Shortlisted
    And I check the status is Sourcing
    And I go to Submissions page
    And I check that submission status is Submitted
    And I'm clicking on Action
    And I check success alert is shown with message The following status has been updated to MSP Reviewed.
    And I check that submission status is MSP Reviewed
    And I'm clicking on Reject Candidate
    And I select reason for rejection as Negative | Candidate Not Local To Position and comments as ABC
    And I check success alert is shown with message The following candidate was rejected successfully.
    And I check the status is Rejected