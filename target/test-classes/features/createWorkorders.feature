@createWorkorders

Feature: Create Workorders

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: PMO creating Workorder
    Given I click on Offers Pending Workorders Creation in dashboard
    When I'm selecting Completed NDA status job
    Then I'm clicking on Create Workorder
    And Job Workorder is created
    And I check success alert is shown with message Success! Workorder created successfully.
    And I check that offer status is Pending Approval - HM