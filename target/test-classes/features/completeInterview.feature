@completeInterview

Feature: Complete Interview

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Interview Completed by Hiring Manager
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Post-Interview Actions in dashboard
    And I go to All Interviews page
    Then I select the approval job with status Approved
#    Then I select Approved Interview Job with name Audit Manager
    And I'm clicking on Interview Completed
    And I select reason as Candidate is fit for the job, planning to make an offer
    And I check success alert is shown with message Interview completed successfully.
    And I check that interview status is Interview Completed