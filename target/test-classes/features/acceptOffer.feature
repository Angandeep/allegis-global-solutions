@acceptOffer

Feature: Accept Offer

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @smoke
  Scenario: Accept Offer by Vendor
    Given I impersonate as Vendor with name Kelly Staffing Agency
    When I click on Pending Offers in dashboard
    Then I select the approval job with status Waiting for Vendor Approval
    And I Accept the offer
    And I check success alert is shown with message You have successfully accepted the Offer.
    And I check that offer status is Approved
    Then I impersonate back as the Program Manager
    And I log out