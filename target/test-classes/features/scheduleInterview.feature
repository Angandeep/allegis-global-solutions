@scheduleInterview

Feature: Schedule Interview

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  @regression
  Scenario: Scheduling Interview by PMO
    When I click on Resumes To Review in dashboard
    Then I select the created job with status Submitted
    And I go to Submissions page
    And I check that submission status is MSP Reviewed
    And I'm clicking on Action
    And I check that submission status is MSP Reviewed
    And I'm clicking on Shortlist
    And I check success alert is shown with message You have successfully Shortlisted a candidate
    And I check that submission status is Shortlisted
    And I Schedule interview
    And I schedule Interview with duration 30 Minutes and type Personal Interview
    And I select slot as 10:00 AM - 10:30 AM
    And I check success alert is shown with message Your Interview Posting was successfully submitted.
    And I check that submission status is Waiting for Approval