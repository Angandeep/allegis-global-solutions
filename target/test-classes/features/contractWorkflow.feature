@contractWorkflow @smoke @regression

Feature: Contract Workflow

  Background: Login as PMO
    Given I login as Program Manager
    Then I'm on the Dashboard page

  Scenario: Hiring Manager Approve Contract Workflow
    Given I impersonate as Hiring Manager with name Angela Ross
    When I click on Contract Amendment Workflows in dashboard
    Then I select the approval job with status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.

  Scenario: VP Approve Contract Workflow
    Given I impersonate as Reporting Manager with name Don LeBert
    When I click on Contract Amendment Workflows in dashboard
    Then I select the approval job with status Pending
    And I Approve the offer
    And I check success alert is shown with message You have approved workflow successfully.